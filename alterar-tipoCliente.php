<?php 
	include 'cabecalho.php';
	include 'conexao/conecta.php';
	include 'bancoTipoCliente.php';

	$id = $_POST['id'];
	$tipoCliente = buscaTipoCliente($con, $id);
?>

<section>
	<div class="container">
		<div class="row">

			<?php 

			/*Variaveis locais*/
			$nomeTipoCliente 	= $_POST['nomeTipoCliente'];


			if (alterarTipoCliente($con, $id, $nomeTipoCliente)) { ?>
			
			<p class="alert bg-success">O usuario <?= $nomeTipoCliente ?>, <?= $nomeTipoCliente ?> foi alterado.</p>
			<?php }else { ?>
			<p class="alert bg-danger">O usuario <?= $nomeTipoCliente ?>, não foi alterado!</p>
			<?php
		}
		?>


	</div>
</div>
</section>

<?php

	include 'footer.php';

 ?>