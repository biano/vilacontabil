<?php include 'cabecalho.php' ?>
<?php include 'conexao/conecta.php'; ?>
<?php include 'bancoEmpresa.php'; ?>

<?php
if (array_key_exists("removido", $_GET) && $_GET["removido"] == "true") {
 ?>
 <section>
  <div class="container">
   <div class="row">
    <p class="bg-success">Empresa cancelada com sucesso!</p>	
  </div>
</div>
</section>
<?php
}
?>


<section>
  <div class="container">
   <div class="row">

    <table class="table table-striped table-bordered">

      <thead>
        <tr>
          <th>Nome d Empresa</th>
          <th>CNPJ</th>
          <th>Nome Responsàvel</th>
          <th>Endereço da Empresa</th>
          <th style="text-align:center">Remover</th>
          <th style="text-align:center">Editar</th>
        </tr>
      </thead>
      <tbody>

       <?php
    /*
     * Esta linha é responsavel por listar e apresentar informaçoes de empresas cadastradas;
     */

    $empresas = listaEmpresas($con);

    foreach ($empresas as $empresa) :
      ?>
    <tr>
      <td><?= $empresa['nome_empresa']; ?></td>
      <td><?= $empresa['cnpj']; ?></td>
      <td><?= $empresa['nome_responsavel']; ?></td>
      <td><?= $empresa['endereco_empresa']; ?></td>
      <td>
        <form action="remove-empresa.php" method="post">
          <input type="hidden" name="id_empresa" value="<?= $empresa['id_empresa']; ?>">
          <button class="btn btn-danger">Remover</button>
        </form>
      </td>
      <td>
        <a class="btn btn-success" href="editar-empresa.php?id=<?= $empresa['id_empresa'] ?>">Editar</a>     
      </td>
    </tr>
    <?php
    endforeach;
    ?>


  </tbody>
</table>
</div>
</div>
</section>

<?php

include 'footer.php'; 

?>