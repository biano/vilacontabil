<?php 
	//Script contendo conteudo referente cabeçalho completo do sistma.
include "cabecalho.php";

//script de conexao com banco.
include 'conexao/conecta.php';

?>


<form id="formAgendamento" action="cadastrarAgendamento.php" data-toggle="validator" role="form">
	<section>

		<div id="conteudo" class="container">
			<div class="row fnd-form">
				<h1>AGENDAMENTO DE ENTREGA</h1>
				<hr />
				<div class="form-group col-md-6">
					<!-- ******************************6******************************************************** -->
					<!--                                  PREENCHER UNIDADE                                     -->
					<!-- ************************************************************************************** -->
					<label for="selecioneUnidade"><h4>Selecione  a unidade</h4></label>
					<select name="unidadeList" class="form-control">
            <option value="0">Escolha uma opção </option>
          <?php 
           
           $resultado = mysqli_query($con, "select * from empresa");
           while ($empresa = mysqli_fetch_assoc($resultado)){ ?>
            <option value="<?= $empresa['id_empresa'] ?>"><?= $empresa['nome_empresa'] ?></option>

           <?php } ?>
						</select>
				</div>
				<div class="form-group col-md-6">
					<label for="selecioneUnidade"><h4>Selecione  Serviço</h4></label>
					<!-- ***************************************************************************************** -->
					<!--                          INDICAR TIPO DE SERVIÇO A SER UTILIZADO                          -->
					<!-- ***************************************************************************************** -->
					<select name="servicoList" class="form-control">
						<option value="0">Escolha uma opção</option>
            <?php
						$resultadoServicos = mysqli_query($con, "select * from servicos");
            while($servico = mysqli_fetch_assoc($resultadoServicos)){ ?>
						<option value="<?= $servico['nome_servico'] ?>"><?= $servico['nome_servico']?></option>
            <?php } ?>
					</select>
				</div>
			</div>
		</div>
	</section>
	<section>
		<!-- ********************************************************************************* -->
		<!--                            INFORMAR EMAIL DO SOLICITANTE DO SERVIÇO               -->
		<!-- ********************************************************************************* -->
		<div class="container">
			<div class="row fnd-form">
				<div class="form-group col-md-6">
					<label for="EmailSolicitante"><h4>Email Solicitante</h4></label>
					<input type="text" name="emailSolicitante" class="form-control" placeholder="Email do Solicitante" data-error="Por favor, informe o email do solicitante." required>
                              <div class="help-block with-errors"></div>

                        </div>
                        <div class="form-group col-md-6">
                             <label for="dataEntrega"><h4>Data de Entrega</h4></label>
                             <input type="date" name="dataEntrega" class="form-control" placeholder="Data de entrega" data-error="Por favor, informar data para entrega." required>
                             <div class="help-block with-errors"></div>  
                       </div>
                 </div>
           </div>

     </section>
     <!-- Email Solicitante - C.custo -->
     <section id="nome-Pac-nm-Prot">
        <!-- **************************************************************************************** -->
        <!--               FORMULARIO DE AGENDAMENTO NOME SOLICITANTE E PROTOCOLO                     -->
        <!-- **************************************************************************************** -->
        <div class="container">
         <div class="row fnd-form">
          <div class="form-group col-md-6">
           <label for="nomeSolicitante"><h4>Nome do Solicitante</h4></label>
           <input type="text" name="nomeSolicitante" class="form-control" placeholder="Nome do Solicitante" data-error="Por favor, Digite o Nome do Solicitante." required>
           <div class="help-block with-errors"></div>  
     </div>
     <div class="form-group col-md-6">
           <label for="protocoloPrincipal"><h4>Numero do Protocolo (Protocolo Principal)</h4></label>
           <input type="text" name="protocoloPrincipal" class="form-control" placeholder="Numero do protocolo" data-error="Por favor, Digite o Numero do Protocolo." required>
           <div class="help-block with-errors"></div> 
     </div>
</div>
</div>
</section>
<!-- Dados Solicitante -->
      <!-- <section>
        <div class="container">
          <div class="row">
            <div class="col-xs-6">
              <div class="form-group">
                <label for="protocoloSolicitante"><h4>CEP </h4></label>
                <input type="text" class="form-control" placeholder="Numero do protocolo">
              </div>
            </div>
            <div class="col-xs-6">
              <div class="form-group">
                <label for="protocoloSolicitante"><h4>CEP Solicitante</h4></label>
                <input type="text" class="form-control" placeholder="Numero do protocolo">
              </div>
            </div>
          </div>
    </section> -->
    <section>
       <!-- *********************************************************************************** -->
       <!--                         AGENDAMENTO CEP, ENDEREÇO, NUMERO                           -->
       <!-- *********************************************************************************** -->
       <div class="container">
        <div class="row fnd-form">
         <div class="form-group col-md-4">
          <label for="CEP"><h4>CEP</h4></label>
          <input type="text" name="cepOrigem" class="form-control cep" placeholder="CEP de origem"
          data-error="Por favor, Digite o CEP de Origem." required>
          <div class="help-block with-errors"></div>              	
    </div>
    <div class="form-group col-md-4">
          <label for="endereco"><h4>Endereco</h4></label>
          <input type="text" name="enderecoOrigem" class="form-control" placeholder="Endereço de origem" data-error="Por favor, Digite o Endereço." required>
          <div class="help-block with-errors"></div>                	
    </div>
    <div class="form-group col-md-4">
          <label for="numero"><h4>Numero</h4></label>
          <input type="text" name="numeroOrigem" class="form-control" placeholder="Numero"
          data-error="Por favor, Digite o Numero." required>
          <div class="help-block with-errors"></div>              	
    </div>
</div>
</div>
</section>
<section>
 <!-- ******************************************************************************** -->
 <!--  AGENDAMENTO CEP SOLICITANTE, ENDEREÇO SOLICITANTE, NUMERO SOLICITANTE           -->
 <!-- ******************************************************************************** -->              
 <div class="container">
  <div class="row fnd-form">
   <div class="form-group col-md-4"> 
    <label for="CEPsolicitante"><h4>CEP Solicitante</h4></label>
    <input type="text" name="cepSolicitante" class="form-control cep" placeholder="CEP Solicitante" data-error="Por favor, Digite o CEP do Solicitante." required>
    <div class="help-block with-errors"></div>							
</div>
<div class="form-group col-md-4">
    <label for="enderecoSolicitante"><h4>Endereco</h4></label>
    <input type="text" name="enderecoSolicitante" class="form-control" placeholder="Endereço do Solicitante" data-error="Por favor, Digite o Endereço." required>
    <div class="help-block with-errors"></div>

</div>
<div class="form-group col-md-4"> 
    <label for="numeroSolicitante"><h4>Numero</h4></label>
    <input type="text" name="numeroSolicitante" class="form-control" placeholder="Numero" data-error="Por favor, Digite o Numero do Solicitante." required>
    <div class="help-block with-errors"></div>
</div>
</div>
</div>
</section>
<section>
 <div class="container">
  <div class="row fnd-form">
   <!-- ***************************************************************************** -->
   <!--       AGENDAMENTO NOME CONTATO OP, NUMERO CONTATO OP, OPERADORA CONTATO OP    -->
   <!-- ***************************************************************************** -->
   <div class="form-group col-md-4">
    <label for="nomeContatoOperacional"><h4>Nome Contato Operacional</h4></label>
    <input type="text" name="nomeContatoOperacional" class="form-control" placeholder="Nome Contato Operacional" data-error="Por favor, Digite o Nome do contato OP." required>
    <div class="help-block with-errors"></div>              	
</div>
<div class="form-group col-md-4">
    <label for="contatoOp1"><h4>Numero Contato Operacional</h4></label>
    <input type="text" name="numeroContatoOperacional" class="form-control telefone" class="telefone" placeholder="Numero Contato Operacional" data-error="Por favor, Digite o Numero do Contato Op." required>
    <div class="help-block with-errors"></div> 

</div>
<div class="form-group col-md-4">
    <label for="nomeOperadoraOp1"><h4>Operadora Contato Operacional</h4></label>
    <input type="text" name="operadoraContatoOperacional" class="form-control" placeholder="Operadora Contato Operaciona" data-error="Por favor, Digite o Nome da Operadora." required>
    <div class="help-block with-errors"></div>              	
</div>
</div>
</div>
</section>
<section>
 <div class="container">
    <!-- ***************************************************************************  -->
    <!--           AGENDAMENTO NOME CONTATO SOL, NUMERO CONTATO SOL, OPERADORA SOL    -->
    <!-- ***************************************************************************  -->
    <div class="row fnd-form">
         <div class="form-group col-md-4">
          <label for="nomecontatoSolicitante"><h4>Nome Contato Solicitante</h4></label>
          <input type="text" name="nomecontatoSolicitante" class="form-control" placeholder="Nome Contato Solicitante" data-error="Por favor, Digite o Nome do Contato." required>
          <div class="help-block with-errors"></div>      	  		
    </div>
    <div class="form-group col-md-4">
          <label for="NumeroContatoSolicitante"><h4>Numero Contato Solicitante</h4></label>
          <input type="text" name="numeroContatoSolicitante" class="form-control telefone" placeholder="Numero Contato Solicitante" data-error="Por favor, Digite o Numero do Contato Solicitante." required>
          <div class="help-block with-errors"></div> 
    </div>
    <div class="form-group col-md-4">
          <label for="nomeOperadoraSolicitante"><h4>Operadora</h4></label>
          <input type="text" name="nomeOperadoraSolicitante" class="form-control" placeholder="Nome Operadora Solicitante" data-error="Por favor, Digite o Nome da Operadora." required>
          <div class="help-block with-errors"></div> 
    </div>
</div>
</div>
</section>

<section>
	<div class="container">
		<div class="row">
			<div class="organiza">
				<button type="submit" formmethod="post" class="btn btn-primary">Enviar</button>
				<button type="reset" class="btn btn-primary">Limpar</button>

			</div>
		</div>
	</div>
</section>

</section>

</form>    <!-- fim agendamento -->




<?php
	//script contendo conteudo referente rodape do sistema.
include "footer.php";

?>
