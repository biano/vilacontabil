<?php 
	include 'cabecalho.php';
	include 'conexao/conecta.php';
	include 'bancoUsuario.php';

	//variaveis locais
	$id = $_GET['id'];
	$usuario = buscaUsuario($con, $id); 
?>

<form name="cadastro-usuario" action="alterar-usuario.php" method="post">
	<input type="hidden" name="id" value="<?=$usuario['codUsuario']?>" >
	<section>
		<div class="container">
			<div class="row fnd-form">
				<div class="form-group col-md-6">
					<label  for="nomeUsuario">Nome Completo</label>
					<input type="text" class="form-control" name="nomeUsuario" value="<?= $usuario['nomeUsuario']?>">
				</div>
				<div class="form-group col-md-6">
					<label  for="emailUsuario">Email</label>
					<input type="text" class="form-control" name="emailUsuario" value="<?= $usuario['emailUsuario']?>">
				</div> 
				<div class="form-group col-md-12">
					<label for="tiposUsuario">Indique o tipo de usuario</label>
					<div>
						<?php
							/*
							* Esta linha é responsavel por listar e apresentar informaçoes de usuarios cadastrados;
							*/
							$resultado = mysqli_query($con, "select * from TblTipos");
							while($tiposUsuario = mysqli_fetch_assoc($resultado)){ ?>
							<label class="checkbox-inline">
								<input type="checkbox" id="inlineCheckbox1" name="codTipos" value="<?=$tiposUsuario['codTipos']?>">
								<?= $tiposUsuario['nomeTipos']?>
							</label>            
							<?php } ?>        
						</div>  
					</div>      
					<div class="form-group col-md-12">
						<label for="passUsuario">Senha</label>
						<input type="password" class="form-control" name="passUsuario" 
						value="<?= $usuario['passUsuario'];?>">
					</div>
				</div>
			</div>
		</section>
		<section>
			<div class="container">
				<div class="row">
					<div class="form-group col-md-12 text-center">
					<button type="submit" class="btn btn-primary">Alterar</button>
						<button type="reset" class="btn btn-primary">Limpar</button>
					</div>
				</div>
			</div>
		</section>
	</form>

	<?php include 'footer.php'; ?>



