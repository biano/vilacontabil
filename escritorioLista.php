<?php include 'cabecalho.php' ?>
<?php include 'conexao/conecta.php'; ?>
<?php include 'bancoEscritorio.php'; ?>

<?php
if (array_key_exists("removido", $_GET) && $_GET["removido"] == "true") {
 ?>
 <section>
  <div class="container">
   <div class="row">
    <p class="bg-success">Escritorio cancelado com sucesso!</p>	
  </div>
</div>
</section>
<?php
}
?>


<section>
  <div class="container">
   <div class="row">

    <table class="table table-striped table-bordered">

      <thead>
        <tr>
          <th>Nome do Escritorio</th>
          <th>CNPJ</th>
          <th>Endereço da Empresa</th>
          <th style="text-align:center">Remover</th>
          <th style="text-align:center">Editar</th>
        </tr>
      </thead>
      <tbody>

       <?php
    /*
     * Esta linha é responsavel por listar e apresentar informaçoes de empresas cadastradas;
     */

    $escritorios = listaEscritorios($con);

    foreach ($escritorios as $escritorio) :
      ?>
    <tr>
      <td><?= $escritorio['nomeEscritorio']; ?></td>
      <td><?= $escritorio['cnpjEscritorio']; ?></td>
      <td><?= $escritorio['enderecoEscritorio']; ?></td>
      <td>
        <form action="remove-escritorio.php" method="post">
          <input type="hidden" name="codEscritorio" value="<?= $escritorio['codEscritorio']; ?>">
          <button class="btn btn-danger">Remover</button>
        </form>
      </td>
      <td>
        <a class="btn btn-success" href="editar-escritorio.php?id=<?= $escritorio['codEscritorio'] ?>">Editar</a>     
      </td>
    </tr>
    <?php
    endforeach;
    ?>


  </tbody>
</table>
</div>
</div>
</section>

<?php

include 'footer.php'; 

?>