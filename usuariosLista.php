<?php include 'cabecalho.php'; ?>
<?php include 'conexao/conecta.php'; ?>
<?php include 'bancoUsuario.php'; ?>

<?php
if (array_key_exists("removido", $_GET) && $_GET["removido"] == "true") {
 ?>
 <section>
  <div class="container">
   <div class="row">
    <p class="text-center text-sucess">Usuario cancelado com sucesso!</p>	
  </div>
</div>
</section>
<?php
}
?>


<section>
  <div class="container">
   <div class="row">

    <table class="table table-striped">
      <thead>
        <tr>
          <th>Nome Completo</th>
          <th>Nome de usuario</th>
          <th>Novo</th>
          <th>Remover</th>
          <th>Editar</th>
        </tr>
      </thead>
      <tbody>



        <?php
          /*
           * Esta linha é responsavel por listar e apresentar informaçoes de usuarios cadastrados;
           */
          $usuarios = listaUsuario($con);

          foreach ($usuarios as $usuario) :
            ?>

          <tr>
            <td><?= $usuario['nomeUsuario']; ?></td>
            <td><?= $usuario['emailUsuario']; ?></td>
            <td><a class="btn btn-primary" href="cadUsuarios.php">Novo</a></td>
            <td>
              <form action="remove-usuarios.php" method="post">
                <input type="hidden" name="codUsuario" value="<?= $usuario['codUsuario']; ?>">
                <button class="btn btn-danger">Remover</button>
              </form>              
            </td>
            <td><a class="btn btn-success" href="editar-usuario.php?id=<?= $usuario['codUsuario'] ?>">Editar</a></td>
          </tr>
          <?php
          endforeach;
          ?>


        </tbody>
      </table>
    </div>
  </div>
</section>

<?php

include 'footer.php'; 

?>