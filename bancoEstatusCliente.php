<?php
/*
 * Função responsavel por listar informações armazenadas na tabela: TblUnidadeAtendimento
 */

function listaEstatusCliente($con) {
    $estatusClientes = array();

    $result = mysqli_query($con, "select * from TblEstatusCliente");
    while ($estatusCliente = mysqli_fetch_assoc($result)) {
        array_push($estatusClientes, $estatusCliente);
    }
    return $estatusClientes;
}

/*
 * Função responsavel por inserir dados na tabela: TblUnidadeAtendimento
 */


function inserirEstatusCliente($con, $nomeEstatusCliente) {
    $query = "insert into TblEstatusCliente (nomeEstatusCliente)";
    $query .= "values('$nomeEstatusCliente')";
    //print $query;
    //die;    
return mysqli_query($con, $query);
}
/*
* Função responsavel por alterar dados na tabela: TblUnidadeAtendimento
*/
function alterarEstatusCliente($con, $id, $nomeEstatusCliente){
    $query = "update TblEstatusCliente set nomeEstatusCliente = '{$nomeEstatusCliente}' where  codEstatusCliente = '{$id}'";
    //print $query;
    //die;
    return mysqli_query($con, $query);
}
/*
* Função responsavel por buscar dados na tabela: TblUnidadeAtendimento
*/
function buscaEstatusCliente($con, $id){
    $query = "select * from TblEstatusCliente where codEstatusCliente = {$id}";
    $resultado = mysqli_query($con, $query);
    return mysqli_fetch_assoc($resultado);
}
/*
 * Funçao responsavel por remover dados da tabela: TblUnidadeAtendimento 
 */

function removeEstatusCliente($con, $id){
    $query = "delete from TblEstatusCliente where codEstatusCliente = {$id}";
    return mysqli_query($con, $query);
    //print $query;
    //die;
    
}