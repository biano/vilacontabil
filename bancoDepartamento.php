<?php

/*
 * Função responsavel por listar informações armazenadas na tabela: TblTipos
 */

function listaDepartamento($con) {
    $departamentos = array();

    $result = mysqli_query($con, "select * from TblDepartamento");
    while ($departamento = mysqli_fetch_assoc($result)) {
        array_push($departamentos, $departamento);
    }
    return $departamentos;
}

/*
 * Função responsavel por inserir dados na tabela: TblTipos
 */


function inserirDepartemento($con, $NomeDepartamento) {
    $query = "insert into TblDepartamento (nomeDepartamento)";
    $query .= "values('$NomeDepartamento')";
    //print $query;
    //die;    
return mysqli_query($con, $query);
}
/*
* Função responsavel por alterar dados na tabela:
*/
function alterarDepartamento($con, $id, $nomeDepartamento){
    $query = "update TblDepartamento set nomeDepartamento = '{$nomeDepartamento}' where  codDepartamento = '{$id}'";
    //print $query;
    //die;
    return mysqli_query($con, $query);
}
/*
* Função responsavel por buscar dados na tabela:
*/
function buscaDepartamento($con, $id){
    $query = "select * from TblDepartamento where codDepartamento = {$id}";
    $resultado = mysqli_query($con, $query);
    return mysqli_fetch_assoc($resultado);
}
/*
 * Funçao responsavel por remover dados da tabela: TblTipos
 */

function removeDepartamento($con, $id){
    $query = "delete from TblDepartamento where codDepartamento = {$id}";
    return mysqli_query($con, $query);   
}