        <ul class="nav navbar-nav">
          <li><a href="home.php">INICIO</a></li>
          <!-- Agendamentos -->
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Administrar<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="pessoaLista.php">Profissionais</a></li>
              <li role="separator" class="divider">
              <li><a href="usuariosLista.php">Usuarios</a></li>
              <li role="separator" class="divider"></li>
              <li><a href="departamentoLista.php">Departamento</a></li>
              <li role="separator" class="divider"></li>
              <li><a href="tipoLista.php">Tipos de Usuarios</a></li>
              <li role="separator" class="divider"></li>
              <li><a href="tipoClienteLista.php">Tipos de Clientes</a>
              <li role="separator" class="divider"></li>
              <li><a href="estatusClienteLista.php">Estatus do cliente</li>
              <li role="separator" class="divider"></li>
              <li><a href="unidadeLista.php">Unidades de atendimento</a>
              <li role="separator" class="divider"></li>
              <li><a href="ramoAtividadeLista.php">Ramo de Atividade</a>
              <!--<li><a href="agendListExp.php">Solicitação Expressa</a></li>-->
            </ul>
          </li>
          <!-- Reclamações -->
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Operacional<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="ccliente.php">Clientes</a></li>
              <li role="separator" class="divider"></li>
              <li><a href="#">Em Desenvolvimento</a></li>
              <li role="separator" class="divider"></li>
              <li><a href="#">Em Desenvolvimento</a></li>              
            </ul>
          </li>
          <!-- Protocolo -->
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Protocolo<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="AcompaPedidos.php">Em Desenvolvimento</a></li>
              <li role="separator" class="divider"></li>
              <li><a href="DetPedidos.php">Em Desenvolvimento</a></li>
              <li role="separator" class="divider"></li>
              <li><a href="OS-Pedidos.php">Em Desenvolvimento</a></li>
            </ul>
          </li>
          <!--Financeiro -->
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Financeiro<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="AcompaPedidos.php">Em Desenvolvimento</a></li>
              <li role="separator" class="divider"></li>
              <li><a href="DetPedidos.php">Em Desenvolvimento</a></li>
              <li role="separator" class="divider"></li>
              <li><a href="OS-Pedidos.php">Em Desenvolvimento</a></li>
            </ul>
          </li>

        </ul>