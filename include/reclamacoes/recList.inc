    <section>
     <div id="conteudo" class="container">
      <div class="row">
       <h1>Consultar Reclamações</h1>
     </div>
   </section>
   
   <section>
    <div class="container">
     <div class="row">
       <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">Panel heading</div>

        <!-- Table -->
        <table class="table">
          <thead>
            <tr>
              <th>Protocolo</th>
              <th>O.S interna</th>
              <th>Nome Completo</th>
              <th style="text-align:center">Ocorrencias</th>
              <th style="text-align:center">Editar</th>
              <th style="text-align:center">Apagar</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><a href="#">1601900</a></td>
              <td><a href="#">0EPG</a></td>
              <td><a href="#">Fulano De Tal</a></td>
              <td align="center"><i class="fa fa-circle fa-2 color-red" aria-hidden="true"></i></td>
              <td align="center"><a href="#"><i class="fa fa-pencil-square fa-3" aria-hidden="true"></i></a></td>
              <td align="center"><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
            </tr>
            <tr>
              <td><a href="#">1601901</a></td>
              <td><a href="#">0EPI</a></td>
              <td><a href="#">Fulano De Tal</a></td>
               <td align="center"><i class="fa fa-circle fa-2 color-yellow" aria-hidden="true"></i></td>
              <td align="center"><a href="#"><i class="fa fa-pencil-square fa-3" aria-hidden="true"></i></td>
              <td align="center"><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></td>
            </tr>
            <tr>
              <td><a href="#">1601902</a></td>
              <td><a href="#">0EPJ</a></td>
              <td><a href="#">Fulano De Tal</a></td>
               <td align="center"><i class="fa fa-circle fa-2 color-red" aria-hidden="true"></i></td>
              <td align="center"><a href="#"><i class="fa fa-pencil-square fa-3" aria-hidden="true"></i></td>
              <td align="center"><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></td>
            </tr>
          </tbody>
        </table>
      </div>      
    </div>
  </div>
</section>

<section>
  <div class="container">
   <div class="row">
    <div class="organiza">
     <button type="submit" class="btn btn-primary">Enviar</button><button type="submit" class="btn btn-primary">Limpar</button>
   </div>
 </div>
</div>
</section>