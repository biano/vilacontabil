<?php 


	//declaraçao de variaveis, responsaveis por retornar o dia da semana * = sabado.
	$data = date('y-m-d');
	$dia_semana = date("w", time($data));
	$semana = array ("Domingo", "Segunda-feira", "Terça-feira", "Quarta-feira", "Quinta-feira", "Sexta-feira", "Sabado");

	//variaveis responsaveis por mostrar os meses do ano;
	$mes = date("m", time($data));
	$meses = array ("Janeiro", "Fevereiro", "Março", "Abril", "Maio", "junho", "julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro");
	
	//Dia do mês representado por numero.
	$dia = date("d", time());
	
	//Ano descrito com 4 digitos
	$ano = date("Y", time());
	 echo "<span>" . $semana[$dia_semana] . "</span>, " . $dia . " de " . $meses[$mes-1] . " " . $ano . ".";
	 