        <ul class="nav navbar-nav">
          <!--Admin. usuarios 1 -->
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Admin. Usuarios<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="cadUsuarios.php">Cadastro Usuários</a></li>
              <li><a href="cadUsuariosList.php">Consultar Usuários</a></li>
              <li role="separator" class="divider"></li>
              <li><a href="pesqUsuarios.php">Pesquisar Usuarios</a></li>              
            </ul>
          </li>
          <!-- Admin. Empresa -->
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Admin. Empresa<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="cadEmpresa.php">Cadastro Empresas</a></li>
              <li><a href="cadEmpresaList.php">Consultar Empresas</a></li>
              <li role="separator" class="divider"></li>
              <li><a href="pesqEmpresa.php">Pesquisar Empresas</a></li>              
            </ul>
          </li>
          <!-- menu 3 -->
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Expedição<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="AcompaPedidos.php">Acompanhamento de pedido</a></li>
              <li><a href="DetPedidos.php">Detalhes do pedido</a></li>
              <li><a href="OS-Pedidos.php">O.S pedido</a></li>
            </ul>
          </li>

        </ul>