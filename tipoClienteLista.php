<?php include 'cabecalho.php' ?>
<?php include 'conexao/conecta.php'; ?>
<?php include 'bancoTipoCliente.php'; ?>

<?php
if (array_key_exists("removido", $_GET) && $_GET["removido"] == "true") {
 ?>
 <section>
  <div class="container">
   <div class="row">
    <p class="bg-success">Tipo de Cliente cancelado com sucesso!</p>	
  </div>
</div>
</section>
<?php
}
?>


<section>
  <div class="container">
   <div class="row">

    <table class="table table-striped table-bordered">

      <thead>
        <tr>
          <th>Tipo de Clientes</th>
          <th width="10%" style="text-align:center">Novo</th>
          <th width="10%" style="text-align:center">Remover</th>
          <th width="10%" style="text-align:center">Editar</th>
        </tr>
      </thead>
      <tbody>

       <?php
    /*
     * Esta linha é responsavel por listar e apresentar informaçoes de empresas cadastradas;
     */
    
    $tipoClientes = listaTipoCliente($con);

    foreach ($tipoClientes as $tipoCliente) :
      ?>
    <tr>
      <td><?= $tipoCliente['nomeTipoCliente']; ?></td>
      <td><a class="btn btn-primary" href="cadTipoCliente.php">Novo</a></td>
      <td>
        <form action="remove-tipoCliente.php" method="post">
          <input type="hidden" name="codTipoCliente" value="<?= $tipoCliente['codTipoCliente']; ?>">
          <button class="btn btn-danger">Remover</button>
        </form>
      </td>
      <td>
        <a class="btn btn-success" href="editar-tipoCliente.php?id=<?= $tipoCliente['codTipoCliente'] ?>">Editar</a>     
      </td>
    </tr>
    <?php
    endforeach;
    ?>


  </tbody>
</table>
</div>
</div>
</section>

<?php

include 'footer.php'; 

?>