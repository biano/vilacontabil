<?php include 'cabecalho.php'; ?>
<?php include 'conexao/conecta.php'; ?>

<form name="cadastro-tipo-cliente" action="cadastrarTipoCliente.php" method="post">
  <section>
    <div class="container">
      <div class="row fnd-form">
        <div class="form-group col-md-12">
          <label  for="nomeTipoCliente">Nome Tipo de Cliente</label>
          <input type="text" class="form-control" name="nomeTipoCliente" placeholder="inform o Tipo de Cliente" data-error="Por favor, Digite o Tipo de Cliente." required>
          <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
          <div class="help-block with-errors"></div>        
        </div>
      </section>
      <section>
        <div class="container">
          <div class="row">
            <div class="form-group col-md-12 text-center">
              <button type="submit" class="btn btn-primary">Cadastrar</button>
              <button type="reset" class="btn btn-primary">Limpar</button>
            </div>
          </div>
        </div>
      </section>
    </form>

    <?php include 'footer.php'; ?>



