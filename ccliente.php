<?php include 'cabecalho.php'; ?>
<?php include 'conexao/conecta.php'; ?>


<section>
	<div id="conteudo" class="container">
		<div class="row">
			<h1>Consulta por Cliente</h1>
		</div>
	</section>
		<section>
			<div class="container">
				<div class="row fnd-form">
					<section class="col-md-12">
						<div class="form-group">
							<label  for="nomeCliente">Nome do Cliente</label>
							<select class="form-control" id="nomeCliente" name="nomeCliente">
								<option value="false" id="nomeCliente" >Selecione um cliente</option>
								  <?php
							          /*
							           * Esta linha é responsavel por listar e apresentar informaçoes de usuarios cadastrados;
							           */
							          $resultado = mysqli_query($con, "select * from TblCliente");
							          while($cliente = mysqli_fetch_assoc($resultado)){ ?>
								<option value="<?= $cliente['nomeCliente']?>" id
									="<?= $cliente['nomeCliente']?>">
            						<?= $cliente['nomeCliente']?>
            					</option>
									  <?php } ?> 
							</select>
						</div>
					</section>
					<section>
						
						<div class="col-md-6 col-md-offset-3">
							<table class="table">
								<thead>
									<tr>
										<th>Obrigaçoes acessorias</th>
										<th>Tarefas</th>
									</tr>
								</thead>
								<tr>
									<td>
										<div class="checkbox disabled">
											<label>
												<input type="checkbox" name="obAces" value="on">Obrigações
											</label>
										</div>	
									</td>
									<td>
									    <div class="checkbox disabled">
											<label>
												<input type="checkbox" name="tarefas" value="on">Tarefas
											</label>
										</div>	
									</td>
								</tr>
							</table>
						</div>
					</section>
				</div>
			</div>
		</section>
		<section>
			<div class="container">
				<div class="row">
					<div class="form-group col-md-12 text-center">
						<button type="submit" id="buscar"  class="btn btn-primary">Pesquisar</button> 
						<button type="reset" class="btn btn-primary">Limpar</button>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div id="result-consulta">
						
					</div>
				</div>
			</div>
		</section>
	</div>
</div>

<!-- </form> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="js/busca.js" type="text/javascript"></script>



<?php include 'footer.php'; ?>
