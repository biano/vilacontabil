<?php include 'cabecalho.php';?>
<?php include 'conexao/conecta.php';?>
<?php include 'bancoUsuario.php'; ?>

<section>
	<div class="container">
		<div class="row">

			<?php 

			/*Variaveis locais*/
			$nomeUsuario 	= $_POST['nomeUsuario'];
			$emailUsuario 	= $_POST['emailUsuario'];
			$tiposUsuario   = $_POST['tiposUsuario']; //tiposusuario_id
			$senhaUsuario 	= $_POST['senhaUsuario'];
			$rsenhaUsuario 	= $_POST['rsenhaUsuario'];

			if (inserirUsuario($con, $nomeUsuario, $emailUsuario, $tiposUsuario, $senhaUsuario, $rsenhaUsuario)) { ?>
			
			<p class="alert-sucess">O usuario <?= $nomeUsuario ?>, <?= $emailUsuario ?> foi cadastrado.</p>
			<?php }else { ?>
			<p class="alert-danger">O usuario <?= $nomeUsuario ?>, não foi cadastrado!</p>
			<?php
		}
		?>


	</div>
</div>
</section>

<?php include 'footer.php'; ?>