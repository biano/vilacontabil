    <?php include 'cabecalho.php'; ?>
    <?php include 'conexao/conecta.php'; ?>
    <?php include 'bancoCliente.php'; ?>

    <?php
        //a chave da matriz existe
    if (array_key_exists("removido", $_GET) && $_GET["removido"] == "true"){

    ?>
   <section>
      <div class="container">
         <div class="row">
            <p class="alert bg-success">Cliente removido com sucesso!</p>   
        </div>
    </div>
</section>
<?php
}
?>
<section>
  <div class="container text-center">

    <h2>Clientes</h2>

    <div class="row">

        <table class="table table-striped table-bordered">

          <thead>
            <tr>
              <th>Nome do Cliente</th>
              <th>CNPJ</th>
              <th>Razão Social</th>
              <th>Email</th>
              <th width="10%" style="text-align:center">Novo</th>
              <th width="10%" style="text-align:center">Remover</th>
              <th width="10%" style="text-align:center">Editar</th>
          </tr>
      </thead>
      <tbody>

         <?php
    /*
     * Esta linha é responsavel por listar e apresentar informaçoes de empresas cadastradas;
     */
    
    $clientes = listaCliente($con);

    foreach ($clientes as $cliente) :
      ?>
      <tr>
          <td><?= $cliente['nomeCliente']; ?></td>
          <td><?= $cliente['cnpjCliente']; ?></td>
          <td><?= $cliente['razaoSocCliente']; ?></td>
          <td><?= $cliente['emailCliente']; ?></td>
          <td><a class="btn btn-primary" href="cadCliente.php">Novo</a></td>
          <td>
            <form action="remove-cliente.php" method="post">
              <input type="hidden" name="codCliente" value="<?= $cliente['codCliente']; ?>">
              <button class="btn btn-danger">Remover</button>
          </form>
      </td>
      <td>
        <a class="btn btn-success" href="editar-cliente.php?id=<?= $cliente['codCliente'] ?>">Editar</a>     
    </td>
</tr>
<?php
endforeach;
?>


</tbody>
</table>
</div>

</div>
</section>

<?php include 'footer.php'; ?>