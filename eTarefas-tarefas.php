<?php include 'cabecalho.php'; ?>
<?php include 'conexao/conecta.php'; ?>


<section>
	<div id="conteudo" class="container">
		<div class="row">
			<h1>Consulta por Tarefas</h1>
		</div>
	</section>

	<form name="cadastro-cliente" action="" method="post">
		<section>
			<div class="container">
				<div class="row fnd-form">
					<section class="col-md-12">
						<div class="form-group">
							<label  for="nomeDepartamento">Nome Departamento</label>
							<select class="form-control" id="nomeCliente" name="nomeCliente">
								<option value="false" >Selecione um Departamento</option>
								  <?php
							          /*
							           * Esta linha é responsavel por listar e apresentar informaçoes de usuarios cadastrados;
							           */
							          $resultado = mysqli_query($con, "select * from TblDepartamento");
							          while($departamento = mysqli_fetch_assoc($resultado)){ ?>
								<option value="<?=$departamento['codDepartamento'];?>">
            						<?php echo "Departamento " . $departamento['nomeDepartamento']?>
            					</option>
									  <?php } ?> 
							</select>
						</div>
					</section>
					<section>
						<div class="col-md-12">
							<div class="form-group">
								<label for=""></label>
							</div>
						</div>
					</section>
					<section>
						<div class="col-md-6">
							<table class="table">
								<thead>
									<tr>
										<th>Obrigaçoes acessorias</th>
										<th>Tarefas</th>
									</tr>
								</thead>
								<tr>
									<td>

										<?php ?>
										<div class="checkbox">
											<label>
												<input type="checkbox" id="obAces"  name="obAces" value="on">Obrigações
											</label>
										</div>	
									</td>
									<td>
									    <div class="checkbox">
											<label>
												<input type="checkbox" name="tarefas" value="on">Tarefas
											</label>
										</div>	
									</td>
								</tr>
							</table>
						</div>
						<div class="col-md-6">
	
						</div>
					</section>
					
				</div>
			</div>
		</section>
		<section>
			<div class="container">
				<div class="row">
					<div class="form-group col-md-12 text-center">
						<button type="submit" id="btn-envia"
						class="btn btn-primary">Pesquisar</button>
						<button type="reset" class="btn btn-primary">Limpar</button>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row">
					
				</div>
			</div>
		</section>
	</div>
</div>

</form>
<script type="text/javascript" src="ajax.js"></script> 




<?php include 'footer.php'; ?>
