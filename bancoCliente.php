<?php

//Funçao valida CPF


 function validaCPF($cpf = null) {

    // Verifica se um número foi informado
    if(empty($cpf)) {
        return false;
    }

    // Elimina possivel mascara
    $cpf = preg_replace("/[^0-9]/", "", $cpf);
    $cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);
    
    // Verifica se o numero de digitos informados é igual a 11 
    if (strlen($cpf) != 11) {
        return false;
    }
    // Verifica se nenhuma das sequências invalidas abaixo 
    // foi digitada. Caso afirmativo, retorna falso
    else if ($cpf == '00000000000' || 
        $cpf == '11111111111' || 
        $cpf == '22222222222' || 
        $cpf == '33333333333' || 
        $cpf == '44444444444' || 
        $cpf == '55555555555' || 
        $cpf == '66666666666' || 
        $cpf == '77777777777' || 
        $cpf == '88888888888' || 
        $cpf == '99999999999') {
        return false;
     // Calcula os digitos verificadores para verificar se o
     // CPF é válido
     } else {   
        
        for ($t = 9; $t < 11; $t++) {
            
            for ($d = 0, $c = 0; $c < $t; $c++) {
                $d += $cpf{$c} * (($t + 1) - $c);
            }
            $d = ((10 * $d) % 11) % 10;
            if ($cpf{$c} != $d) {
                return false;
            }
        }

        return true;
    }
}

/*
 * Função responsavel por listar informações armazenadas na tabela: cliente */

function listaCliente($con) {
    $clientes = array();

    $result = mysqli_query($con, "select * from TblCliente");
    while ($cliente = mysqli_fetch_assoc($result)) {
        array_push($clientes, $cliente);
    }
    return $clientes;
}

/*
 * Função responsavel por inserir dados na tabela: cliente */


function inserirCliente($con, $nomeCliente, $razaoSocCliente, $codTipoCliente, $codEstatusCliente, $cnpjCliente,$cnpjInicioCliente, $dataFundacaoCliente, 
    $inscEstCliente, $inscMunCliente, $telefoneCliente,
    $celularCliente, $emailCliente, $siteCliente, $codRamoAtuacao, 
    $codUnidade, $enderecoCliente, $numeroEndCliente,$referenciaCliente, $bairroCliente, 
    $cidadeCliente, $ufCliente, $cepCliente, $enderecoCobranca) {
    $query = "insert into TblCliente (nomeCliente, razaoSocCliente, codTipoCliente, codEstatusCliente, cnpjCliente,cnpjInicioCliente, dataFundacaoCliente,  inscEstCliente, inscMunCliente, telefoneCliente, celularCliente, emailCliente, siteCliente, codRamoAtuacao, codUnidade, enderecoCliente, numeroEndCliente, referenciaCliente, bairroCliente, cidadeCliente, ufCliente, cepCliente, enderecoCobranca)";

    $query .= "values('$nomeCliente','$razaoSocCliente',$codTipoCliente, 
         $codEstatusCliente, '$cnpjCliente','$cnpjInicioCliente', '$dataFundacaoCliente', '$inscEstCliente', '$inscMunCliente', '$telefoneCliente', '$celularCliente', '$emailCliente', 
        '$siteCliente', $codRamoAtuacao, $codUnidade, '$enderecoCliente', 
        '$numeroEndCliente', '$referenciaCliente', '$bairroCliente', '$cidadeCliente', 
        '$ufCliente', '$cepCliente', '$enderecoCobranca')";
    print $query;
    die;    
    return mysqli_query($con, $query);
}
/*
* Função responsavel por alterar dados na tabela:
*/

function alterarCliente($con, $id, $nomeCliente, $razaoSocCliente, $codTipoCliente, $codEstatusCliente, $cnpjCliente,$cnpjInicioCliente, $dataFundacaoCliente, 
    $inscEstCliente, $inscMunCliente, $telefoneCliente,
    $celularCliente, $emailCliente, $siteCliente, $codRamoAtuacao, 
    $codUnidade, $enderecoCliente, $numeroEndCliente, $referenciaCliente, $bairroCliente, 
    $cidadeCliente, $ufCliente, $cepCliente, $enderecoCobranca){

    $query = "update TblCliente set nomeCliente = '{$nomeCliente}', razaoSocCliente = '{$razaoSocCliente}', codTipoCliente = {$codTipoCliente}, codEstatusCliente = {$codEstatusCliente}, cnpjCliente = '{$cnpjCliente}', cnpjInicioCliente = '{$cnpjInicioCliente}', dataFundacaoCliente = '{$dataFundacaoCliente}', inscEstCliente = '{$inscEstCliente}', inscMunCliente = '{$inscMunCliente}', telefoneCliente ='{$telefoneCliente}', celularCliente = '{$celularCliente}', emailCliente = '{$emailCliente}', siteCliente = '{$siteCliente}', codRamoAtuacao = {$codRamoAtuacao}, codUnidade = {$codUnidade}, enderecoCliente = '{$enderecoCliente}',numeroEndCliente = '{$numeroEndCliente}',referenciaCliente = '{$referenciaCliente}', bairroCliente = '{$bairroCliente}', cidadeCliente ='$cidadeCliente', ufCliente = '{$ufCliente}', cepCliente = '{$cepCliente}', enderecoCobranca = '{$enderecoCobranca}'";

    //print $query;
    //die;
    return mysqli_query($con, $query);

            
}
/*
* Função responsavel por buscar dados na tabela:
*/
function buscaCliente($con, $id){
    $query = "select * from TblCliente where codCliente = {$id}";
    $resultado = mysqli_query($con, $query);
    return mysqli_fetch_assoc($resultado);
}
/*
 * Funçao responsavel por remover dados da tabela: agendamento
 */

function removeCliente($con, $id){
    $query = "delete from TblCliente where codCliente = {$id}";
    return mysqli_query($con, $query);
    
}

