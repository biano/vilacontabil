<?php 
include 'cabecalho.php';
include 'conexao/conecta.php';
include 'bancoPessoa.php'; 


	//variaveis locais
$id = $_GET['id'];
$pessoas = buscaPessoa($con, $id);
?>



<section>
	<div id="conteudo" class="container">
		<div class="row">
			<h1>Alterar dados de Pessoas</h1>
		</div>
	</section>
	<!-- arquivo que testa conexao com o banco -->

	<form id="formPessoa" action="alterar-pessoa.php" method="POST">
		<input type="hidden" name="id" value="<?=$pessoas['codPessoa']?>">
		<section>
			<div class="container">
				<div class="row fnd-form">
					<div class="form-group col-md-6">
						<label for="nomePessoa" class="control-label"><h4>Nome completo</h4></label>
						<input type="text" class="form-control" id="nomePessoa" name="nomePessoa" value="<?= $pessoas['nomePessoa']?>">
					</div>
					<div class="form-group col-md-6">
						<label for="emailPessoa" class="control-label"><h4>Email</h4></label>
						<input type="text" class="form-control" id="emailPessoa" name="emailPessoa" value="<?= $pessoas['emailPessoa']?>">
					</div>
				
				<div class="form-group col-md-12">
					<label for="codsexo" class="control-label"><h4>Sexo</h4></label>
					<select id="codsexo" name="codSexo" class="form-control">

						<?php

						$query = "SELECT * FROM TblSexo";
						$result = $con->query($query);

						if ($result->num_rows > 0) {
				                // output data of each row
							while($row = $result->fetch_assoc()) {
								?>
								<option value="<?= $row['codSexo']; ?>"> <?= $row['nomeSexo']; ?> </option>
								<?php			       
							}
						} else {
							echo "0 results";
						}
						$con->close();
						?>

					</select>
				</div>
				<div class="form-group col-md-6">
					<label for="rgPessoa" class="control-label"><h4>RG</h4></label>
					<input type="text" class="form-control" id="rgPessoa" name="rgPessoa" value="<?= $pessoas['rgPessoa']?>">
				</div>
				<div class="form-group col-md-6">
					<label for="cpfPessoa" class="control-label"><h4>CPF</h4></label>
					<input type="text" class="form-control" id="cpfPessoa" name="cpfPessoa" value="<?= $pessoas['cpfPessoa']?>">
				</div>
				<div class="form-group col-md-6">
					<label for="celularfonePessoa" class="control-label"><h4>Celular</h4></label>
					<input type="text" class="form-control" id="celularPessoa" name="celularPessoa" value="<?= $pessoas['celularPessoa']?>">
				</div>
				<div class="form-group col-md-6">
					<label for="TelefonePessoa" class="control-label"><h4>Telefone</h4></label>
					<input type="text" class="form-control" id="TelefonePessoa" name="TelefonePessoa" value="<?= $pessoas['TelefonePessoa']?>">
				</div>
				<div class="form-group col-md-4">
					<label for="enderecoPessoa" class="control-label"><h4>Endereço</h4></label>
					<input type="text" class="form-control" id="enderecoPessoa" name="enderecoPessoa" value="<?= $pessoas['enderecoPessoa']?>">
				</div>
				<div class="form-group col-md-4">
					<label for="numeroPessoa" class="control-label"><h4>Numero</h4></label>
					<input type="text" class="form-control" id="numeroPessoa" name="numeroPessoa" value="<?= $pessoas['numeroPessoa']?>">
				</div>
				<div class="form-group col-md-4">
					<label for="" class="control-label"><h4>Bairro</h4></label>
					<input type="text" class="form-control" id="bairroessoa" name="bairroPessoa" value="<?= $pessoas['bairroPessoa']?>">
				</div>
				<div class="form-group col-md-4">
					<label for="" class="control-label"><h4>CEP</h4></label>
					<input type="text" class="form-control" id="cepPessoa" name="cepPessoa" value="<?= $pessoas['cepPessoa']?>">
				</div>
				<div class="form-group col-md-4">
					<label for="" class="control-label"><h4>Cidade</h4></label>
					<input type="text" class="form-control" id="cidadePessoa" name="cidadePessoa" value="<?= $pessoas['cidadePessoa']?>">
				</div>
				<div class="form-group col-md-4">
					<label for="" class="control-label"><h4>UF</h4></label>
					<input type="text" class="form-control" id="ufPessoa" name="ufPessoa" value="<?= $pessoas['ufPessoa']?>">
				</div>
			</div>
		</div>
		</div>
	</section>

	<section>
		<div class="container">
			<div class="row">
				<div class="organiza">
					<button type="submit" class="btn btn-primary">Alterar</button><button type="submit" class="btn btn-primary">Limpar</button>
				</div>
			</div>
		</div>
	</section>
</form>






<?php include 'footer.php'; ?>