<?php

/*
 * Função responsavel por listar informações armazenadas na tabela: cliente */

function listaRamoAtividade($con) {
    $ramoAtividades = array();

    $result = mysqli_query($con, "select * from TblRamoAtividade");
    while ($ramoAtividade = mysqli_fetch_assoc($result)) {
        array_push($ramoAtividades, $ramoAtividade);
    }
    return $ramoAtividades;
}
/*
 * Função responsavel por inserir dados na tabela: cliente */


function inserirRamoAtividade($con, $nomeRamoAtividade) {
    $query = "insert into TblRamoAtividade (nomeRamoAtividade)";

    $query .= "values($nomeRamoAtividade)";
    print $query;
    die;    
    return mysqli_query($con, $query);
}
/*
* Função responsavel por alterar dados na tabela:
*/
function alterarRamoAtividade($con, $id, $nomeRamoAtividade){
    $query = "update TblRamoAtividade set nomeRamoAtividade = '{$nomeRamoAtividade}' where  codRamoAtividade = '{$id}'";
    //print $query;
    //die;
    return mysqli_query($con, $query);
}
/*
* Função responsavel por buscar dados na tabela:
*/
function buscaRamoAtividade($con, $id){
    $query = "select * from TblRamoAtividade where codRamoAtividade = {$id}";
    //print $query;
    //die;
    $resultado = mysqli_query($con, $query);
    return mysqli_fetch_assoc($resultado);
}
/*
 * Funçao responsavel por remover dados da tabela: TblTipos
 */

function removeRamoAtividade($con, $id){
    $query = "delete from TblRamoAtividade where codRamoAtividade = {$id}";
    return mysqli_query($con, $query);   
}