 
<?php include 'cabecalho.php'; ?>
<?php include 'conexao/conecta.php';?>
<?php include 'bancoEscritorio.php'; ?>

<section>
	<div class="container">
		<div class="row">

			<?php

			$nomeEscritorio 		= $_POST['nomeEscritorio'];
			$cnpjEscritorio 		= $_POST['cnpjEscritorio'];
			$enderecoEscritorio 	= $_POST['enderecoEscritorio'];
			$bairroEscritorio 		= $_POST['bairroEscritorio'];
			$referenciaEscritorio 	= $_POST['referenciaEscritorio'];
			$cepEscritorio          = $_POST['cepEscritorio'];
			$cidadeEscritorio		= $_POST['cidadeEscritorio']; 
			$estadoEscritorio       = $_POST['estadoEscritorio'];
			$ufEscritorio           = $_POST['ufEscritorio'];

			if(inserirEscritorio($con, $nomeEscritorio, $cnpjEscritorio,$enderecoEscritorio,$bairroEscritorio, $referenciaEscritorio, $cepEscritorio, $cidadeEscritorio, $estadoEscritorio, $ufEscritorio)){ ?>
			<h2>Cadastro de Escritorio</h2>
			<p class="alert bg-sucess">O Escritorio <?= $nomeEscritorio ?> foi cadastrado.</p>
			<?php }else { ?>
			<h2>Cadastro de Escritorio</h2>
			<p class="bg-danger">O usuario <?= $nomeEscritorio ?>, não foi cadastrado!</p>
			<?php
		}
		?>
		</div>
	</div>
</section>


<?php include 'footer.php'; ?>