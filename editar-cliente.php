<?php include 'cabecalho.php'; ?>
<?php include 'conexao/conecta.php'; ?>
<?php include 'bancoCliente.php'; ?>

<?php

	//variaveis locais
	$id = $_GET['id'];
	$cliente = buscaCliente($con, $id);
?>


<form name="cadastro-cliente" action="alterar-cliente.php" method="post">
	<input type="hidden" name="id" value="<?=$cliente['codCliente']?>">
	<section>
		<div class="container">
			<div class="row fnd-form">
				<section class="col-md-12">
					<div class="form-group">
						<label  for="nomeCliente">Nome do Cliente</label>
						<input type="text" class="form-control" name="nomeCliente" value="<?= $cliente['nomeCliente']?>" placeholder="inform o nome do cliente" data-error="Por favor, Digite o nome do Cliente!" required>
						<div class="help-block with-errors"></div>
					</div>
				</section>
				<section>
					<div class="col-md-12">
						<div class="form-group">
							<label  for="razaoSocCliente">Razão Social</label>
							<input type="text" class="form-control" name="razaoSocCliente"
							value="<?= $cliente['razaoSocCliente']?>" placeholder="Razão Social" data-error="Por favor, Informe a Razão Social!" required>
							<div class="help-block with-errors"></div>
						</div>
					</div>	
				</section>

				<section>
					<div class="col-md-6">
						<div class="form-group">
							<label  for="codTipoCliente">Tipo de Cliente</label>
							<select class="form-control" name="codTipoCliente">
								<option>Escolha uma opção:</option>
								<?php
							          /*
							           * Esta linha é responsavel por listar e apresentar informaçoes de usuarios cadastrados;
							           */
							          $resultado = mysqli_query($con, "select * from TblTipoCliente");
							          while($tipoCliente = mysqli_fetch_assoc($resultado)){ 
							          	?>
							          	<option value="<?=$tipoCliente['codTipoCliente'];?>">
							          		<?= $tipoCliente['nomeTipoCliente']?>
							          	</option>
							          <?php } ?>
							      </select>
							  </div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label  for="codEstatusCliente">Estatus do cliente</label>
									<select class="form-control" name="codEstatusCliente">
										<option>Escolha uma opção:</option>
										<?php
							          /*
							           * Esta linha é responsavel por listar e apresentar informaçoes de usuarios cadastrados;
							           */
							          $resultado = mysqli_query($con, "select * from TblEstatusCliente");
							          while($estatusCliente = mysqli_fetch_assoc($resultado)){ 
							          	?>
							          	<option value="<?=$estatusCliente['codEstatusCliente'];?>">
							          		<?= $estatusCliente['nomeEstatusCliente']?>
							          	</option>
							          <?php } ?>
							      </select>						
							  </div>
							</div>
						</section>
						<section>
							<div class="col-md-4">
								<div class="form-group">
									<label  for="cnpjCliente">CNPJ</label>
									<input type="text" class="form-control" name="cnpjCliente" value="<?= $cliente['cnpjCliente']?>" maxlength="14" placeholder="informe o CNPJ" data-error="Por favor, Digite o CNPJ." required>
									<div class="help-block with-errors"></div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label  for="cnpjInicioCliente">Inicio CNPJ</label>
									<input type="date" class="form-control" name="cnpjInicioCliente" value="<?= $cliente['cnpjInicioCliente']?>"  placeholder="8 digitos iniciais do CNPJ" data-error="Por favor, 8 digitos iniciais do CNPJ." required>
									<div class="help-block with-errors"></div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label  for="dataFundacaoCliente">Data de Fundação</label>
									<input type="date" class="form-control" name="dataFundacaoCliente" value="<?= $cliente['dataFundacaoCliente']?>" placeholder="00/00/0000" data-error="Por favor, informa a data." required>
									<div class="help-block with-errors"></div>
								</div>
							</div>
						</section>
						<section>
							<hr class="divider">
							<div class="col-md-6">
								<div class="form-group">
									<label  for="inscEstCliente">Inscrição Estadual</label>
									<input type="text" class="form-control" name="inscEstCliente" value="<?= $cliente['inscEstCliente']?>" placeholder="Inscrição Estadual" data-error="Por favor, Inscrição Estadual." required>
									<div class="help-block with-errors"></div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label  for="inscMunCliente">Inscrição Municipal</label>
									<input type="text" class="form-control" name="inscMunCliente" value="<?= $cliente['inscMunCliente']?>" placeholder="Inscrição Municipal" data-error="Por favor, Inscrição Municipal." required>
									<div class="help-block with-errors"></div>
								</div>
							</div>	
						</section>
						<section>
							<div class="col-md-3">
								<div class="form-group">
									<label  for="telefoneCliente">Telefone</label>
									<input type="text" class="form-control" value="<?= $cliente['telefoneCliente']?>" name="telefoneCliente" maxlength="10" placeholder="Informe seu telefone" data-error="Por favor, Informe seu telefone!" required>
									<div class="help-block with-errors"></div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label  for="celularCliente">Celular</label>
									<input type="text" class="form-control" name="celularCliente" value="<?= $cliente['celularCliente']?>" maxlength="11" placeholder="Informe seu Celular" data-error="Por favor, Informe seu Celular!" required>
									<div class="help-block with-errors"></div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label  for="emailCliente">Email</label>
									<input type="text" class="form-control" name="emailCliente" value="<?= $cliente['emailCliente']?>" placeholder="Email" data-error="Por favor, Informe seu Email!" required>
									<div class="help-block with-errors"></div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label  for="siteCliente">Site</label>
									<input type="text" class="form-control" name="siteCliente" value="<?= $cliente['siteCliente']?>" placeholder="Informe o site" data-error="Por favor, Informe seu site!" required>
									<div class="help-block with-errors"></div>
								</div>						
							</div>
						</section>
						<section>

							<div class="col-md-6">
								<div class="form-group">
									<label  for="codRamoAtuacao">Ramo de Atividade</label>
									<select type="text" class="form-control" name="codRamoAtuacao">
										<option>Selecione uma Opção:</option>
										<?php
							          /*
							           * Esta linha é responsavel por listar e apresentar informaçoes de usuarios cadastrados;
							           */
							          $resultado = mysqli_query($con, "select * from TblRamoAtividade");
							          while($ramoAtividade = mysqli_fetch_assoc($resultado)){ 
							          	?>
							          	<option value="<?=$ramoAtividade['codRamoAtividade'];?>">
							          		<?= $ramoAtividade['nomeRamoAtividade']?>
							          	</option>
							          <?php } ?>
							      </select>
							      <div class="help-block with-errors"></div>
							  </div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label  for="codUnidade">Unidade</label>
									<select type="text" class="form-control" name="codUnidade">
										<option>Selecione uma Opção:</option>
										<?php
							          /*
							           * Esta linha é responsavel por listar e apresentar informaçoes de usuarios cadastrados;
							           */
							          $resultado = mysqli_query($con, "select * from TblUnidadeAtendimento");
							          while($unidades = mysqli_fetch_assoc($resultado)){ 
							          	?>
							          	<option value="<?=$unidades['codUnidade'];?>">
							          		<?= $unidades['nomeUnidade']?>
							          	</option>
							          <?php } ?>

							      </select>
							  </div>
							</div>
						</section>
						<section>
							<div class="form-group">
								<div class="col-md-4">
									<label  for="enderecoCliente">Endereço</label>
									<input type="text" class="form-control" name="enderecoCliente" value="<?= $cliente['enderecoCliente']?>" placeholder="Informe seu endereço" data-error="Por favor, Informe seu seu endereço!" required>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-2">
									<label  for="numeroEndCliente">Numero</label>
									<input type="text" class="form-control" name="numeroEndCliente" value="<?= $cliente['numeroEndCliente']?>" placeholder="Informe o numero endereço" data-error="Por favor, Informe o numero do endereço!" required>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-6">
									<label  for="referenciaCliente">Referencia</label>
									<input type="text" class="form-control" name="referenciaCliente" value="<?= $cliente['referenciaCliente']?>" placeholder="Informe uma referencia" data-error="Por favor, Informe uma referência!" required>
								</div>
							</div>
						</section>
						<section>
							<div class="col-md-3">
								<div class="form-group">
									<label  for="cepCliente">CEP</label>
									<input type="text" class="form-control" name="cepCliente" value="<?= $cliente['cepCliente']?>" placeholder="Informe uma referencia" data-error="Por favor, Informe uma referência!" required>
								</div>
							</div>					
							<div class="col-md-3">
								<div class="form-group">
									<label  for="bairroCliente">Bairro</label>
									<input type="text" class="form-control" name="bairroCliente" value="<?= $cliente['bairroCliente']?>" placeholder="Informe uma referencia" data-error="Por favor, Informe uma referência!" required>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<label  for="cidadeCliente">Cidade</label>
									<input type="text" class="form-control" name="cidadeCliente" value="<?= $cliente['cidadeCliente']?>" placeholder="Informe uma referencia" data-error="Por favor, Informe uma referência!" required>
								</div>
							</div>
							<div class="col-md-1">
								<div class="form-group">
									<label  for="ufCliente">UF</label>
									<input type="text" class="form-control" name="ufCliente" value="<?= $cliente['ufCliente']?>" placeholder="Informe uma referencia" data-error="Por favor, Informe uma referência!" required>
								</div>
							</div>	
						</section>
						<section>
							<div class="col-md-12">
								<div class="form-group">

									<label for="enderecoCobranca">Endereço de cobrança</label>
									<input type="text" class="form-control" name="enderecoCobranca" value="<?= $cliente['enderecoCliente']?>" placeholder="informe endereço de Cobrança." data-error="por favor informe endereço de cobrança!">
								</div>
							</div>
						</section>

					</section>
					<section>
						<div class="container">
							<div class="row">
								<div class="form-group col-md-12 text-center">
									<button type="submit" class="btn btn-primary">Cadastrar</button>
									<button type="reset" class="btn btn-primary">Limpar</button>
								</div>
							</div>
						</div>
					</section>
				</div>
			</div>

		</form>




		<?php include 'footer.php'; ?>
