<?php 
	include 'cabecalho.php';
	include 'conexao/conecta.php';
	include 'bancoUsuario.php';

	$id = $_POST['id'];
	$usuario = buscaUsuario($con, $id);
?>

<section>
	<div class="container">
		<div class="row">

			<?php 

			/*Variaveis locais*/
			$nomeUsuario 	= $_POST['nomeUsuario'];
			$emailUsuario 	= $_POST['emailUsuario'];
			$codTipos   = $_POST['codTipos']; //tiposusuario_id
			$passUsuario 	= $_POST['passUsuario'];
			//$rsenhaUsuario 	= $_POST['rsenhaUsuario'];

			if (alterarUsuario($con, $id, $nomeUsuario, $emailUsuario, $codTipos, 
				$passUsuario)) { ?>
			
			<p class="bg-success">O usuario <?= $nomeUsuario ?>, <?= $emailUsuario ?> foi alterado.</p>
			<?php }else { ?>
			<p class="bg-danger">O usuario <?= $nomeUsuario ?>, não foi alterado!</p>
			<?php
		}
		?>


	</div>
</div>
</section>

<?php

	include 'footer.php';

 ?>