<?php include 'cabecalho-index.php'; ?>
<?php include 'conexao/conecta.php'; ?>

<section>
  <div class="container">
    <div class="row fnd-form">
      <form name="form-autentica" action="valida.php" method="post" data-toggle="validator" role="form">
        <div class="form-group col-md-12 text-center">
          <h2>Acesso Restrito</h2>

          <h2 class="alert-danger">Senha Invalida!</h2>

        </div>
        <div class="form-group col-md-6">
          <label for="NomeUser" class="control-label"><h4>Informe o nome de usuario:</h4></label>
          <input type="text" class="form-control" id="nomeUsuario" name="nomeUsuario" placeholder="Nome de Usuario" data-error="Por favor, informe o nome do usuario." required>
          <div class="help-block with-errors"></div>
        </div> 
        <div class="form-group col-md-6">
          <label for="password" class="control-label"><h4>Informe sua Senha</h4></label>
          <input type="password" class="form-control" name="passUsuario" id="passUsuario" placeholder="Digite sua senha" data-error="Por favor, informe sua senha." required>
          <div class="help-block with-errors"></div>
        </div>
      </div>
    </div>
  </section>
  <section>
    <div class="container">
      <div class="row text-center">
        <button type="submit" class="btn btn-primary">Acessar</button>
        <button type="reset" class="btn btn-primary">Limpar</button>              
      </div>
    </div>
  </section> 
</form>

<?php include 'footer.php'; ?>