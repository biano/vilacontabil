<?php include 'cabecalho.php'; ?>
<?php include 'conexao/conecta.php';?>
<?php include 'bancoTipos.php'; ?>

<section>
	<div class="container">
		<div class="row">

			<?php 

			/*Variaveis locais*/
			$nomeTipos 	= $_POST['nomeTipos'];

			if (inserirTipo($con, $nomeTipos)) { ?>
			
			<p class="alert-sucess">Novo tipo usuario <?= $nomeTipos ?> foi cadastrado.</p>
			<?php }else { ?>
			<p class="alert-danger">O tipo de usuario <?= $nomeTipos ?>, não foi cadastrado!</p>
			<?php
		}
		?>


	</div>
</div>
</section>

<?php include 'footer.php'; ?>