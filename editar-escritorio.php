<?php include 'cabecalho.php'; ?>
<?php include 'conexao/conecta.php'; ?>
<?php include 'bancoEscritorio.php'; ?>

<?php
  //variaveis locais
$id = $_GET['id'];
$escritorio = buscaEscritorio($con, $id);
?>
<form name="editar-escritorio" action="alterar-escritorio.php" method="post">

  <section>

    <div class="container">
      <div class="row fnd-form">
        <h2 class="text-center">Cadastrar Escritorio</h2>
        <div class="form-group col-md-12">
          <label  for="nomeEscritorio">Nome do Escritorio</label>
          <input type="text" class="form-control" name="nomeEscritorio" value="<?=$escritorio['nomeEscritorio']?>">
        </div>
        <div class="form-group col-md-12">
          <label  for="cnpjEscritorio">CNPJ</label>
          <input type="text" class="form-control" name="cnpjEscritorio" value="<?=$escritorio['cnpjEscritorio']?>"
        </div>
        <div class="form-group col-md-12">
          <label  for="enderecoEscritorio">Endereço do Escritorio</label>
          <input type="text" class="form-control" name="enderecoEscritorio" value="<?=$escritorio['enderecoEscritorio']?>">
        </div>
        <div class="form-group col-md-12">
          <label  for="bairroEscritorio">Bairro</label>
          <input type="text" class="form-control" name="bairroEscritorio" value="<?=$escritorio['bairroEscritorio']?>">
        </div>
        <div class="form-group col-md-12">
          <label  for="referenciaEscritorio">Referencia</label>
          <input type="text" class="form-control" name="referenciaEscritorio"  value="<?=$escritorio['referenciaEscritorio']?>">
        </div>
        <div class="form-group col-md-12">
          <label  for="cepEscritorio">CEP</label>
          <input type="text" class="form-control" name="cepEscritorio" value="<?=$escritorio['cepEscritorio']?>">
        </div>
        <div class="form-group col-md-12">
          <label  for="cidadeEscritorio">Cidade</label>
          <input type="text" class="form-control" name="cidadeEscritorio"  value="<?=$escritorio['cidadeEscritorio']?>">
        </div>
        <div class="form-group col-md-12">
          <label  for="estadoEscritorio">Estado</label>
          <input type="text" class="form-control" name="estadoEscritorio"  value="<?=$escritorio['estadoEscritorio']?>">
        </div>
        <div class="form-group col-md-12">
          <label  for="ufEscritorio">UF</label>
          <input type="text" class="form-control" name="ufEscritorio" value="<?=$escritorio['ufEscritorio']?>">
      </div>
    </div>
  </section>
  <section>
    <div class="container">
      <div class="row">
        <div class="form-group col-md-12 text-center">
          <button type="submit" class="btn btn-primary">Cadastrar</button>
          <button type="reset" class="btn btn-primary">Limpar</button>
        </div>
      </div>
    </div>
  </section>
</form>

<?php include 'footer.php'; ?>



