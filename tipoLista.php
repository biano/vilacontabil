<?php include 'cabecalho.php' ?>
<?php include 'conexao/conecta.php'; ?>
<?php include 'bancoTipos.php'; ?>

<?php
if (array_key_exists("removido", $_GET) && $_GET["removido"] == "true") {
 ?>
 <section>
  <div class="container">
   <div class="row">
    <p class="bg-success">Tipo de usuario cancelado com sucesso!</p>	
  </div>
</div>
</section>
<?php
}
?>


<section>
  <div class="container">
   <div class="row">

    <table class="table table-striped table-bordered">

      <thead>
        <tr>
          <th>Tipo de Usuario</th>
          <th width="10%" style="text-align:center">Novo</th>
          <th width="10%" style="text-align:center">Remover</th>
          <th width="10%" style="text-align:center">Editar</th>
        </tr>
      </thead>
      <tbody>

       <?php
    /*
     * Esta linha é responsavel por listar e apresentar informaçoes de empresas cadastradas;
     */
    
    $tipos = listaTipos($con);

    foreach ($tipos as $tipo) :
      ?>
    <tr>
      <td><?= $tipo['nomeTipos']; ?></td>
      <td><a class="btn btn-primary" href="cadTipoUsuarios.php">Novo</a></td>
      <td>
        <form action="remove-tipo.php" method="post">
          <input type="hidden" name="codTipos" value="<?= $tipo['codTipos']; ?>">
          <button class="btn btn-danger">Remover</button>
        </form>
      </td>
      <td>
        <a class="btn btn-success" href="editar-tipo.php?id=<?= $tipo['codTipos'] ?>">Editar</a>     
      </td>
    </tr>
    <?php
    endforeach;
    ?>


  </tbody>
</table>
</div>
</div>
</section>

<?php

include 'footer.php'; 

?>