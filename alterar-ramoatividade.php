 
<?php include 'cabecalho.php'; ?>
<?php include 'conexao/conecta.php';?>
<?php include 'bancoRamoAtividade.php'; ?>
<?php
	$id = $_POST['id'];
	$ramoAtividade = buscaRamoAtividade($con, $id);
?>
<section>
	<div class="container">
		<div class="row">

				<?php


				$nomeRamoAtividade = $_POST['nomeRamoAtividade'];

				if(alterarRamoAtividade($con,$id,$nomeRamoAtividade)){ ?>
				    <p class="alert alert-success">Ramo de Atividade <span><?= $nomeRamoAtividade ?></span> foi cadastrado.</p>
				<?php }else { ?>
				    <p class="alert alert-danger">Ramo de Atividade <span><?= $nomeRamoAtividade ?></span>, não foi cadastrado!</p>
				<?php
				}
				?>

			
		</div>
	</div>
</section>


<?php include 'footer.php'; ?>