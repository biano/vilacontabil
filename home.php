    <?php include 'cabecalho.php'; ?>
    <?php include 'conexao/conecta.php'; ?>
    <section>
      <div class="container text-center">

        <h2>Bem vindo ao vila Contabil</h2>
        <p>Meus atalhos</p>
        <div class="row">
          <a href="departamentoLista.php">
            <div class="col-md-4">
              <div class="pricing color1">
                <h3>Parametros</h3>
                <div class="prici">
                  <i class="fa fa-asterisk fa-4x" aria-hidden="true"></i>
                </div>
                <p>Programa de Trabalho</p>
              </div>
            </div>
          </a>
          <a href="clientesLista.php">
            <div class="col-md-4">
              <div class="pricing color2">
                <h3>Operacional</h3>
                <div class="prici">
                  <span class="fa fa-list-alt fa-4x" aria-hidden="true"></span>
                </div>
                <p>Meus Clientes</p>
              </div>
            </div>
          </a>
          <a href="procuracoesLista.php">
            <div class="col-md-4">
              <div class="pricing color3">
                <h3>Procurações</h3>
                <div class="prici">
                  <span class="fa fa-file fa-4x" aria-hidden="true"></span>
                </div>
                <p>Vencimentos</p>
              </div>
            </div>
          </a>
          <a href="desepesasLista.php">
            <div class="col-md-4">
              <div class="pricing color1">
                <h3>Financeiro</h3>
                <div class="prici">
                  <span class="fa  fa-paperclip fa-4x" aria-hidden="true"></span>
                </div>
                <p>Lançamento de Despesas</p>
              </div>
            </div>
          </a>
          <a href="faturamentoLista.php">
            <div class="col-md-4">
              <div class="pricing color2">
                <h3>Financeiro</h3>
                <div class="prici">
                  <span class="fa fa-usd fa-4x" aria-hidden="true"></span>
                </div>
                <p>Faturamento</p>
              </div>
            </div>
          </a>
          <a href="relatorioLista.php">
            <div class="col-md-4">
              <div class="pricing color3">
                <h3>Operacional</h3>
                <div class="prici">
                  <span class="fa fa-dashboard fa-4x" aria-hidden="true"></span>
                </div>
               <p>Gestão de Tarefas</p>
              </div>
            </div>
          </a>
        </div>
      </div>
    </section>

    <?php include 'footer.php'; ?>