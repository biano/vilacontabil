<?php include 'cabecalho.php'; ?>
<?php include 'conexao/conecta.php';?>
<?php include 'bancoTipoCliente.php'; ?>

<section>
	<div class="container">
		<div class="row">

			<?php 

			/*Variaveis locais*/
			$nomeTipoCliente 	= $_POST['nomeTipoCliente'];

			if (inserirTipoCliente($con, $nomeTipoCliente)) { ?>
			
			<p class="alert alert-sucess">Novo tipo usuario <?= $nomeTipoCliente ?> foi cadastrado.</p>
			<?php }else { ?>
			<p class="alert alert-danger">O tipo de usuario <?= $nomeTipoCliente ?>, não foi cadastrado!</p>
			<?php
		}
		?>
	</div>
</div>
</section>

<?php include 'footer.php'; ?>