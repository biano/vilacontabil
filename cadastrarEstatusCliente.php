<?php include 'cabecalho.php'; ?>
<?php include 'conexao/conecta.php';?>
<?php include 'bancoEstatusCliente.php'; ?>

<section>
	<div class="container">
		<div class="row">

			<?php 

			/*Variaveis locais*/
			$nomeEstatusCliente 	= $_POST['nomeEstatusCliente'];

			if (inserirEstatusCliente($con, $nomeEstatusCliente)) { ?>
			
			<p class="alert alert-sucess">Novo tipo usuario <?= $nomeEstatusCliente ?> foi cadastrado.</p>
			<?php }else { ?>
			<p class="alert alert-danger">O tipo de usuario <?= $nomeEstatusCliente ?>, não foi cadastrado!</p>
			<?php
		}
		?>
	</div>
</div>
</section>

<?php include 'footer.php'; ?>