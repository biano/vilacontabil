<?php

/*
 * Função responsavel por listar informações armazenadas na tabela: TblEscritorio */

function listaEscritorios($con) {
    $escritorios = array();

    $result = mysqli_query($con, "select * from TblEscritorio");
    while ($escritorio = mysqli_fetch_assoc($result)) {
        array_push($escritorios, $escritorio);
    }
    return $escritorios;
}

/*
 * Função responsavel por inserir dados na tabela: TblEscritorio
 */

function inserirEscritorio($con, $nomeEscritorio, $cnpjEscritorio, $enderecoEscritorio, $bairroEscritorio,$referenciaEscritorio, $cepEscritorio, $cidadeEscritorio, $estadoEscritorio, $ufEscritorio) {
    $query = "INSERT INTO TblEscritorio (nomeEscritorio, cnpjEscritorio, enderecoEscritorio, bairroEscritorio,referenciaEscritorio, cepEscritorio, cidadeEscritorio, estadoEscritorio, ufEscritorio) VALUES ('$nomeEscritorio', '$cnpjEscritorio', '$enderecoEscritorio', '$bairroEscritorio', '$referenciaEscritorio', '$cepEscritorio', '$cidadeEscritorio', '$estadoEscritorio', '$ufEscritorio')";

return mysqli_query($con, $query);
}

/*
* Função responsavel por alterar dados na tabela: TblPessoa
*/
function alterarEscritorio($con, $id, $nomeEscritorio, $cnpjEscritorio, $enderecoEscritorio, $bairroEscritorio,$referenciaEscritorio, $cepEscritorio, $cidadeEscritorio, $estadoEscritorio, $ufEscritorio){
    $query = "update TblEscritorio set nomeEscritorio = '{$nomeEscritorio}',cnpjEscritorio = '{$cnpjEscritorio}', enderecoEscritorio = '{$enderecoEscritorio}', bairroEscritorio = '{$bairroEscritorio}',referenciaEscritorio = '{$referenciaEscritorio}', cepEscritorio = '{$cepEscritorio}', cidadeEscritorio = '{$cidadeEscritorio}', estadoEscritorio = '{$estadoEscritorio}', ufEscritorio = '{$ufEscritorio}' where codEscritorio = '{$id}'";
    return mysqli_query($con, $query);
}
/*
* Função responsavel por buscar dados na tabela: tblPessoa
*/
function buscaEscritorio($con, $id){
    $query = "select * from TblEscritorio where codEscritorio = {$id}";
    $resultado = mysqli_query($con, $query);
    return mysqli_fetch_assoc($resultado);
    //print $query;
    //die;
}
/*
 * Funçao responsavel por remover dados da tabela: TblPessoa
 */

function removeEscritorio($con, $id){
    $query = "delete from TblEscritorio where codEscritorio = {$id}";
    //print $query;
    //die;
    return mysqli_query($con, $query);   
}