<?php include 'cabecalho.php'; ?>
<?php include 'conexao/conecta.php'; ?>
<?php include 'bancoUnidade.php'; ?>
<?php include 'bancoEscritorio.php'; ?>

<form name="cadastro-unidadeAtendimento" action="cadastrarUnidade.php" method="post">

  <section>

    <div class="container">
      <div class="row fnd-form">
        <h2 class="text-center">Cadastrar Unidade de Atendimento</h2>
        <div class="form-group col-md-12">
          <label  for="nomeUnidade">Nome da Unidade Atendimento</label>
          <input type="text" class="form-control" name="nomeUnidade" placeholder="inform o nome da Unidade de Atendimento" data-error="Por favor, Digite o nome Unidade de Atendimento." required>
          <hr>
          <label for="codEscritorio">Matriz do Escritorio</label>
          <select class="form-control" name="codEscritorio">

            <?php 
            $escritorios = listaEscritorios($con);
            foreach ($escritorios as $escritorio) : 
              //if($escritorios == "true"){ ?>
                <option value="<?= $escritorio['codEscritorio']; ?>"><?= $escritorio['nomeEscritorio']; ?></option>

              <?php//} else { ?>

                <option><?php// echo "Não há dados para ser apresentado!"; ?></option>

              <?php// } ?>
          
              <?php
            endforeach;
            ?>
          </select>
        </div>
      </div>
    </div>
  </section>
  <section>
    <div class="container">
      <div class="row">
        <div class="form-group col-md-12 text-center">
          <button type="submit" class="btn btn-primary">Cadastrar</button>
          <button type="reset" class="btn btn-primary">Limpar</button>
        </div>
      </div>
    </div>
  </section>
</form>

<?php include 'footer.php'; ?>



