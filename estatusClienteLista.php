<?php include 'cabecalho.php' ?>
<?php include 'conexao/conecta.php'; ?>
<?php include 'bancoEstatusCliente.php'; ?>

<?php
if (array_key_exists("removido", $_GET) && $_GET["removido"] == "true") {
 ?>
 <section>
  <div class="container">
   <div class="row">
    <p class="bg-success">Estatus de Cliente cancelado com sucesso!</p>	
  </div>
</div>
</section>
<?php
}
?>


<section>
  <div class="container">
   <div class="row">

    <table class="table table-striped table-bordered">

      <thead>
        <tr>
          <th>Estatus do Cliente</th>
          <th width="10%" style="text-align:center">Novo</th>
          <th width="10%" style="text-align:center">Remover</th>
          <th width="10%" style="text-align:center">Editar</th>
        </tr>
      </thead>
      <tbody>

       <?php
    /*
     * Esta linha é responsavel por listar e apresentar informaçoes de empresas cadastradas;
     */
    
    $estatusClientes = listaestatusCliente($con);

    foreach ($estatusClientes as $estatusCliente) :
      ?>
    <tr>
      <td><?= $estatusCliente['nomeEstatusCliente']; ?></td>
      <td><a class="btn btn-primary" href="cadEstatusCliente.php">Novo</a></td>
      <td>
        <form action="remove-estatusCliente.php" method="post">
          <input type="hidden" name="codEstatusCliente" value="<?= $estatusCliente['codEstatusCliente']; ?>">
          <button class="btn btn-danger">Remover</button>
        </form>
      </td>
      <td>
        <a class="btn btn-success" href="editar-estatusCliente.php?id=<?= $estatusCliente['codEstatusCliente'] ?>">Editar</a>     
      </td>
    </tr>
    <?php
    endforeach;
    ?>


  </tbody>
</table>
</div>
</div>
</section>

<?php

include 'footer.php'; 

?>