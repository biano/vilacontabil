<?php
/*
 * Função responsavel por listar dados de usuarios da tabela: TblUsuarios
 */
function listaUsuario($con) {
    $usuarios = array();
    $result = mysqli_query($con, "select * from TblUsuarios");
    while ($usuario = mysqli_fetch_assoc($result)) {
        array_push($usuarios, $usuario);
    }
    return $usuarios;
}

/*
 * Função responsavel por cadastrar dados de usuarios na tabela: TblUsuarios */

function inserirUsuario($con, $nomeUsuario, $emailUsuario, $tiposUsuario, $passUsuario) {
    $query = "insert into TblUsuarios (nomeUsuario,emailUsuario,codTipos,passUsuario)";
    $query .= "values ('$nomeUsuario','$emailUsuario', '$tiposUsuario', '$passUsuario')";

    //print $query;
    //die;

    return mysqli_query($con, $query);
}
/*
* Função responsavel por alterar dados de usuario na tabela: TblUsuarios*/
function alterarUsuario($con, $id, $nomeUsuario, $emailUsuario, $tiposUsuario, $passUsuario){
    $query = "update TblUsuarios set nomeUsuario ='{$nomeUsuario}', emailUsuario ='{$emailUsuario}', codTipos ='{$tiposUsuario}', passUsuario ='{$passUsuario}' where codUsuario = {$id}";
    //print $query;
    //die;

    return mysqli_query($con, $query);

}
/*
* Função responsavel por buscar dados da tabela TblUsuarios
*/

function buscaUsuario($con, $id){
    $query = "select * from TblUsuarios where codUsuario = {$id}";
    $resultado = mysqli_query($con, $query);
    return mysqli_fetch_assoc($resultado);
}

/*
 * Funçao responsavel por remover dados da tabela: TblUsuarios
*/
function removeUsuarios($con, $id){
    $query = "delete from TblUsuarios where codUsuario = {$id}";
    return mysqli_query($con, $query);
    
}
