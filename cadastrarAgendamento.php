<?php include 'cabecalho.php'; ?>

<?php include 'conexao/conecta.php';?>
<?php include 'bancoAgendamento.php'; ?>

<section>
    <div class="container">
     <div class="row">


<?php
/*Funcao limpa caracteres deixando apenas numeros*/
function soNumero($str) {
    return preg_replace("/[^0-9]/", "", $str);
}

/* 
 * Variaveis locais;
 */

$unidadeList 					= $_POST['unidadeList'];
$servicoList 					= $_POST['servicoList'];
$emailSolicitante 				= $_POST['emailSolicitante'];
$nomeSolicitante 				= $_POST['nomeSolicitante'];
$cepOrigem 						= soNumero($_POST['cepOrigem']);
$enderecoOrigem 				= $_POST['enderecoOrigem'];
$numeroOrigem 					= $_POST['numeroOrigem'];
$nomeContatoOperacional 		= $_POST['nomeContatoOperacional'];
$numeroContatoOperacional 		= soNumero($_POST['numeroContatoOperacional']);
$operadoraContatoOperacional 	= $_POST['operadoraContatoOperacional'];
$dataEntrega 					= date("Y-m-d", strtotime($_POST['dataEntrega']));
$protocoloPrincipal 			= $_POST['protocoloPrincipal'];
$cepSolicitante 				= soNumero($_POST['cepSolicitante']);
$enderecoSolicitante  			= $_POST['enderecoSolicitante'];
$numeroSolicitante            	= $_POST['numeroSolicitante'];
$nomecontatoSolicitante       	= $_POST['nomecontatoSolicitante'];
$numeroContatoSolicitante     	= soNumero($_POST['numeroContatoSolicitante']);
$nomeOperadoraSolicitante     	= $_POST['nomeOperadoraSolicitante'];
$dataAtendimento 			  	= date('Y-m-d'); //data do dia




/*
.=unidadeList,servicoList,emailSolicitante,nomeSolicitante,cepOrigem,enderecoOrigem,numeroOrigem
.=nomeContatoOperacional,numeroContatoOperacional,operadoraContatoOperacional,centroCusto,protocoloPrincipal
.=cepSolicitante,enderecoSolicitante,numeroSolicitante,nomecontatoSolicitante,numeroContatoSolicitante,nomeOperadoraSolicitante
*/
if(inserirAgendamento(
	$con,$unidadeList, $servicoList, $emailSolicitante, $nomeSolicitante, $cepOrigem, $enderecoOrigem, $numeroOrigem, $nomeContatoOperacional, $numeroContatoOperacional, $operadoraContatoOperacional, $dataEntrega, $protocoloPrincipal, $cepSolicitante, $enderecoSolicitante, $numeroSolicitante, $nomecontatoSolicitante, $numeroContatoSolicitante, $nomeOperadoraSolicitante, $dataAtendimento)){ ?>
    <p class="alert-sucess text-center">A Solicitação do Sr: <?= $nomeSolicitante ?>, portador do Protocolo numero: <?= $protocoloPrincipal ?> foi cadastrado.</p>
<?php }else { ?>
    <p class="alert-danger">O usuario <?= $nomeSolicitante ?>, não foi cadastrado!</p>
<?php
}
?>

</div>
</div>
</section>



<?php

	include 'footer.php';

 ?>