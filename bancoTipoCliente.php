<?php
/*
 * Função responsavel por listar informações armazenadas na tabela: TblUnidadeAtendimento
 */

function listaTipoCliente($con) {
    $tipoClientes = array();

    $result = mysqli_query($con, "select * from TblTipoCliente");
    while ($tipoCliente = mysqli_fetch_assoc($result)) {
        array_push($tipoClientes, $tipoCliente);
    }
    return $tipoClientes;
}

/*
 * Função responsavel por inserir dados na tabela: TblUnidadeAtendimento
 */


function inserirTipoCliente($con, $nomeTipoCliente) {
    $query = "insert into TblTipoCliente (nomeTipoCliente)";
    $query .= "values('$nomeTipoCliente')";
    //print $query;
    //die;    
return mysqli_query($con, $query);
}
/*
* Função responsavel por alterar dados na tabela: TblUnidadeAtendimento
*/
function alterarTipoCliente($con, $id, $nomeTipoCliente){
    $query = "update TblTipoCliente set nomeTipoCliente = '{$nomeTipoCliente}' where  codTipoCliente = '{$id}'";
    //print $query;
    //die;
    return mysqli_query($con, $query);
}
/*
* Função responsavel por buscar dados na tabela: TblUnidadeAtendimento
*/
function buscaTipoCliente($con, $id){
    $query = "select * from TblTipoCliente where codTipoCliente = {$id}";
    $resultado = mysqli_query($con, $query);
    return mysqli_fetch_assoc($resultado);
}
/*
 * Funçao responsavel por remover dados da tabela: TblUnidadeAtendimento 
 */

function removeTipoCliente($con, $id){
    $query = "delete from TblTipoCliente where codTipoCliente = {$id}";
    return mysqli_query($con, $query);
    //print $query;
    //die;
    
}