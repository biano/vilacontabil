<?php include 'cabecalho.php'; ?>

<section>
<div class="container">
	<div class="row">
		<form id="formExemplo" action="f-molde-form.php" data-toggle="validator" role="form">
  			<div class="form-group col-md-6">
    			<label for="textTelefone" class="control-label">Telefone</label>
    			<input name="telefone" class="form-control telefone" placeholder="Digite o numero do telefone..." type="text" data-error="Por favor, informe o numero do telefone." required>
            	<div class="help-block with-errors"></div>
  			</div>
			<div class="form-group col-md-6">
    			<label for="textCPF" class="control-label">CPF</label>
    			<input name="cpf" class="form-control cpf" placeholder="Digite o numero do cpf" type="text" data-error="Por favor, informe um numero valido de cpf." required>
            <div class="help-block with-errors"></div>
  			</div>
  			<div class="form-group col-md-6">
  				<button type="submit" formmethod="post" class="btn btn-primary" name="enviar">Enviar</button>
  			</div>
		</form>
	</div>
</div>
</section>
<?php include 'footer.php'; ?>