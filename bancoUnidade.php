<?php

/*
 * Função responsavel por listar informações armazenadas na tabela: TblUnidadeAtendimento
 */

function listaUnidades($con) {
    $unidades = array();

    $result = mysqli_query($con, "select * from TblUnidadeAtendimento");
    while ($unidade = mysqli_fetch_assoc($result)) {
        array_push($unidades, $unidade);
    }
    return $unidades;
}

/*
 * Função responsavel por inserir dados na tabela: TblUnidadeAtendimento
 */


function inserirUnidade($con, $nomeUnidade, $codEscritorio) {
    $query = "insert into TblUnidadeAtendimento (nomeUnidade, codEscritorio)";
    $query .= "values('$nomeUnidade', '$codEscritorio')";
    //print $query;
    //die;    
return mysqli_query($con, $query);
}
/*
* Função responsavel por alterar dados na tabela: TblUnidadeAtendimento
*/
function alterarUnidade($con, $id, $nomeUnidade){
    $query = "update TblUnidadeAtendimento set nomeUnidade = '{$nomeUnidade}' where  codUnidade = '{$id}'";
    //print $query;
    //die;
    return mysqli_query($con, $query);
}
/*
* Função responsavel por buscar dados na tabela: TblUnidadeAtendimento
*/
function buscaUnidade($con, $id){
    $query = "select * from TblUnidadeAtendimento where codUnidade = {$id}";
    $resultado = mysqli_query($con, $query);
    return mysqli_fetch_assoc($resultado);
}
/*
 * Funçao responsavel por remover dados da tabela: TblUnidadeAtendimento 
 */

function removeUnidade($con, $id){
    $query = "delete from TblUnidadeAtendimento where codUnidade = {$id}";
    return mysqli_query($con, $query);
    //print $query;
    //die;
    
}