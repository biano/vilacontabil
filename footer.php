
  
  <footer>
   <div id="footer-center" class="container">
    <div class="row">

      <h4> VILA CONTABIL ® - <?php echo date("Y"); ?></h4>
          <!--ul>
            <li>Suporte Técnico</li>
            <li>Contato</li>
            <li>contato@vilacontabil.com.br</li>
          </ul-->

    </div>
  </div>
</footer>


<!-- Masked input -->
<script src="assets/js/jquery.maskedinput.min.js"/></script>

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/validator.js"></script>

<script>
   
  $(document).ready(function(){
    $(".telefone").mask("(99)99999-9999");
    $(".cpf").mask("999.999.999-99");
    $(".cep").mask("99999-999");
    $(".data").mask("99/99/9999");
  });
    $('#formAgendamento').validator();
    $('#formEmpresa').validator();
    $('#form_reclamacoes').validator();
    $('#form-pesq-rec').validator();
    $('#form-autentica').validator();
</script>
</body>
</html>