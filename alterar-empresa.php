<?php 
include 'cabecalho.php'; 
include 'conexao/conecta.php';
include 'bancoEmpresa.php'; 

$id = $_POST['id'];
$empresa = buscaEmpresa($con, $id);

?>

<section>
	<div class="container">
		<div class="row">

				<?php 

				function somente_numero( $n ) { return preg_replace("/[^0-9]/", "", $n);}
				
				$NomeEmpresa = $_POST['NomeEmpresa'];
				$cnpj = somente_numero($_POST['cnpj']);
				$NomeResponsavel = $_POST['NomeResponsavel'];
				$TelefoneEmpresa = somente_numero( $_POST['TelefoneEmpresa'] );
				$CelularEmpresa = somente_numero( $_POST['CelularEmpresa'] );
				$EnderecoEmpresa = $_POST['EnderecoEmpresa'];
				$NumeroEmpresa = somente_numero( $_POST['NumeroEmpresa'] );
				$BairroEmpresa = $_POST['BairroEmpresa'];
				$ReferenciaEmpresa = $_POST['ReferenciaEmpresa'];
				$NomeContatoEmpresa = $_POST['NomeContatoEmpresa'];
				$EmailContatoEmpresa = $_POST['EmailContatoEmpresa'];

				
				if(alterarEmpresa($con, $id, $NomeEmpresa,$cnpj, $NomeResponsavel, $NomeContatoEmpresa, $EmailContatoEmpresa, $TelefoneEmpresa, $CelularEmpresa, $EnderecoEmpresa, $NumeroEmpresa, $BairroEmpresa, $ReferenciaEmpresa)){ ?>
				    <p class="bg-success">Empresa <?= $NomeEmpresa ?>, <?= $NomeResponsavel ?> foi alterada.</p>
				<?php }else { ?>
				    <p class="bg-danger">Empresa <?= $NomeEmpresa ?>, não foi alterada!</p>
				<?php
				}
				?>

			
		</div>
	</div>
</section>


<?php include 'footer.php'; ?>