<?php

/*
 * Função responsavel por listar informações armazenadas na tabela: TblTipos
 */

function listaTipos($con) {
    $tipos = array();

    $result = mysqli_query($con, "select * from TblTipos");


    while ($tipo = mysqli_fetch_assoc($result)) {
        array_push($tipos, $tipo);
    }
    return $tipos;

}


/*
 * Função responsavel por inserir dados na tabela: TblTipos
 */


function inserirTipo($con, $nomeTipos) {
    $query = "insert into TblTipos (nomeTipos)";
    $query .= "values('$nomeTipos')";
    //print $query;
    //die;    
return mysqli_query($con, $query);
}
/*
* Função responsavel por alterar dados na tabela:
*/
function alterarTipo($con, $id, $nomeTipos){
    $query = "update TblTipos set nomeTipos = '{$nomeTipos}' where  codTipos = '{$id}'";
    //print $query;
    //die;
    return mysqli_query($con, $query);
}
/*
* Função responsavel por buscar dados na tabela:
*/
function buscaTipo($con, $id){
    $query = "select * from TblTipos where codTipos = {$id}";
    $resultado = mysqli_query($con, $query);
    return mysqli_fetch_assoc($resultado);
}
/*
 * Funçao responsavel por remover dados da tabela: TblTipos
 */

function removeTipos($con, $id){
    $query = "delete from TblTipos where codTipos = {$id}";
    return mysqli_query($con, $query);
    //print $query;
    //die;
    
}