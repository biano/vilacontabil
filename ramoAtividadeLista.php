<?php include 'cabecalho.php' ?>
<?php include 'conexao/conecta.php'; ?>
<?php include 'bancoRamoAtividade.php'; ?>

<?php
if (array_key_exists("removido", $_GET) && $_GET["removido"] == "true") {
 ?>
 <section>
  <div class="container">
   <div class="row">
    <p class="bg-success">Ramo Atividade cancelado com sucesso!</p>	
  </div>
</div>
</section>
<?php
}
?>


<section>
  <div class="container">
   <div class="row">

    <table class="table table-striped table-bordered">

      <thead>
        <tr>
          <th>Ramo de Atividade</th>
          <th width="10%" style="text-align:center">Novo</th>
          <th width="10%" style="text-align:center">Remover</th>
          <th width="10%" style="text-align:center">Editar</th>
        </tr>
      </thead>
      <tbody>

       <?php
    /*
     * Esta linha é responsavel por listar e apresentar informaçoes de empresas cadastradas;
     */
    
    $ramoAtividades = listaRamoAtividade($con);

    foreach ($ramoAtividades as $ramoAtividade) :
      ?>
    <tr>
      <td><?= $ramoAtividade['nomeRamoAtividade']; ?></td>
      <td><a class="btn btn-primary" href="cadRamoAtividade.php">Novo</a></td>
      <td>
        <form action="remove-ramoatividade.php" method="post">
          <input type="hidden" name="codRamoAtividade" value="<?= $ramoAtividade['codRamoAtividade']; ?>">
          <button class="btn btn-danger">Remover</button>
        </form>
      </td>
      <td>
        <a class="btn btn-success" href="editar-ramoatividade.php?id=<?= $ramoAtividade['codRamoAtividade'] ?>">Editar</a>     
      </td>
    </tr>
    <?php
    endforeach;
    ?>


  </tbody>
</table>
</div>
</div>
</section>

<?php

include 'footer.php'; 

?>