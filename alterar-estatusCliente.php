<?php 
	include 'cabecalho.php';
	include 'conexao/conecta.php';
	include 'bancoEstatusCliente.php';

	$id = $_POST['id'];
	$estatusCliente = buscaEstatusCliente($con, $id);
?>

<section>
	<div class="container">
		<div class="row">

			<?php 

			/*Variaveis locais*/
			$nomeEstatusCliente 	= $_POST['nomeEstatusCliente'];


			if (alterarEstatusCliente($con, $id, $nomeEstatusCliente)) { ?>
			
			<p class="alert bg-success">O usuario <?= $nomeEstatusCliente ?>, <?= $nomeEstatusCliente ?> foi alterado.</p>
			<?php }else { ?>
			<p class="alert bg-danger">O usuario <?= $nomeEstatusCliente ?>, não foi alterado!</p>
			<?php
		}
		?>


	</div>
</div>
</section>

<?php

	include 'footer.php';

 ?>