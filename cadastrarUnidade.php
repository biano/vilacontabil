 
<?php include 'cabecalho.php'; ?>
<?php include 'conexao/conecta.php';?>
<?php include 'bancoUnidade.php'; ?>

<section>
	<div class="container">
		<div class="row">

				<?php


				$nomeUnidade = $_POST['nomeUnidade'];
				$codEscritorio = $_POST['codEscritorio'];

				if(inserirUnidade($con,$nomeUnidade,$codEscritorio)){ ?>
				    <p class="alert alert-success">Unidade de Atendimento <span><?= $nomeUnidade ?></span> foi cadastrado.</p>
				<?php }else { ?>
				    <p class="alert alert-danger">Unidade de Atendimento <span><?= $nomeUnidade ?></span>, não foi cadastrado!</p>
				<?php
				}
				?>

			
		</div>
	</div>
</section>


<?php include 'footer.php'; ?>