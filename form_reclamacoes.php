
<section>
 <div id="conteudo" class="container">
  <div class="row">
   <h1>Registro de Reclamações</h1>
 </div>
</section>
<form name="form_reclamacoes" method="post" action="" data-toggle="validator" role="form">
 <section>
  <div class="container">
  <div class="row fnd-form">
     <div class="form-group col-md-12">
      <label for="selecioneUnidade"><h4>Protocolo atendimento</h4></label>
      <input type="text" class="form-control" placeholder="Informe o codigo de  Atendimento" data-error="Por favor, informe protocolo de atendimento." required>
      <div class="help-block with-errors"></div>
    </div>
  </div>
</div>
</section>
<section id="laboratorio">
  <div class="container">
   <div class="row fnd-form">
     <div class="form-group col-md-12">
      <label for="selecioneUnidade"><h4>Selecione  a unidade</h4></label>
      <select class="form-control" data-error="teste" required>
       <option value="0">Escolha uma opção </option>
        <?php
         $resultado = mysqli_query($con, "select * from empresa");
         while ($empresa = mysqli_fetch_assoc($resultado)) { ?>
          <option value="<?= $empresa['id_empresa'] ?>"><?= $empresa['nome_empresa'] ?></option>
         <?php } ?>
     </select>
     <div class="help-block with-errors"></div>
   </div>
 </div>
</div>
</section>
<section>
 <div class="container">
   <div class="row fnd-form">
     <div class="form-group col-md-12">
      <label for="Novacorrencia"><h4>Ocorrencia</h4></label>
      <input type="text" class="form-control" placeholder="Informe o codigo da ocorrencia" data-error="Por favor, informe um titulo para esta ocorrência." required>
      <div class="help-block with-errors"></div>
    </div>
  </div>
</div>
</section>
<section>
  <div class="container">
   <div class="row fnd-form">
     <div class="form-group col-lg-12">
      <label for="selecioneUnidade"><h4>Descrição completa da ocorrencia.</h4></label>
      <textarea class="form-control" rows="3" placeholder="Descreva toda ocorrencia." data-error="Por favor, descreva detalhes da ocorrencia." required>
      </textarea>
      <div class="help-block with-errors"></div>
    </div>
  </div>
</div>
</section>
<section>
  <div class="container">
   <div class="row fnd-form">
     <div class="form-group col-lg-12">
      <div class="form-group">
       <label for="exampleInputFile">Anexar Arquivo</label>
       <input type="file" id="exampleInputFile">
       <p class="help-block">Selecione arquivo na maquina</p>
     </div>
   </div>
 </div>
</div>
</section>
<section>
  <div class="container">
   <div class="row">
    <div class="organiza">
     <button type="submit" class="btn btn-primary">Enviar</button><button type="submit" class="btn btn-primary">Limpar</button>
   </div>
 </div>
</div>
</section>
</form>

