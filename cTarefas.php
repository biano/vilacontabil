<?php include 'cabecalho.php'; ?>
<?php include 'conexao/conecta.php'; ?>


<section>
	<div id="conteudo" class="container">
		<div class="row">
			<h1>Consulta por Tarefa</h1>
		</div>
	</section>
	<section>
		<div class="container">
			<div class="row fnd-form">
				<section class="col-md-12">
					<div class="form-group">
						<label  for="nomeCliente">Nome Departamento</label>
						<select class="form-control" id="codDepartamento" name="codDepartamento">
							<option value="false" id="codDepartamento" value="codDepartamento" name="codDepartamento">
								Selecione um Departamento...
							</option>
							<?php
							          /*
							           * Esta linha é responsavel por listar e apresentar informaçoes de usuarios cadastrados;
							           */
							          $resultado = mysqli_query($con, "select * from TblDepartamento");
							          while($departamento = mysqli_fetch_assoc($resultado)){ ?>
							          	<option value="<?= $departamento['codDepartamento']?>" id="<?= $departamento['codDepartamento']?>">
							          		<?= $departamento['nomeDepartamento']?>
							          	</option>
							          <?php } ?> 
							      </select>
							  </div>
							  <div class="form-group">
							  	<div id="result-tarefas">

							  	</div>
							  </div>
							</section>
							<section>
								<div class="col-md-6 col-md-offset-3">
									<div class="checkbox-inline">
										<label>
											<input type="checkbox" name="checks[]" id="tipo" value="1"> 1
										</label>
									</div>
									<div class="checkbox-inline">
										<input type="checkbox" name="checks[]" id="tipo" value="2"> 2
									</div>
									<div class="checkbox-inline">
										<input type="checkbox" name="checks[]" id="tipo" value="3"> 3
									</div>
								</div>
							</section>
						</div>
					</div>
				</section>
				<section>
					<div class="container">
						<div class="row">
							<div class="form-group col-md-12 text-center">
								<button type="submit" id="buscar"  class="btn btn-primary">Pesquisar</button> 
								<button type="reset" class="btn btn-primary">Limpar</button>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>

		<!-- </form> -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="js/buscaTarefa.js" type="text/javascript"></script>



		<?php include 'footer.php'; ?>