
	
	function buscar(nomeCliente){
		var page = "busca.php";

		//declaraçao das variaveis
		var codCliente  = $("#codCliente").val();

		$.ajax({
			type: 'POST',
			dataType: 'html',
			url: page,

			beforeSend: function(){

				$("#result-consulta").html("Carregando ...");
			},

			data: {nomeCliente: nomeCliente},
			success: function (msg){
				$("#result-consulta").html(msg);
			} 
		});
	}

	$('#buscar').click(function(){

		buscar($("#nomeCliente").val())

	})

