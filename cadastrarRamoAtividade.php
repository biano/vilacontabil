<?php include 'cabecalho.php'; ?>
<?php include 'conexao/conecta.php';?>
<?php include 'bancoRamoAtividade.php'; ?>

<section>
	<div class="container">
		<div class="row">

			<?php 

			/*Variaveis locais*/
			$nomeRamoAtividade 	= $_POST['nomeRamoAtividade'];

			if (inserirRamoAtividade($con, $nomeRamoAtividade)) { ?>
			
			<p class="alert alert-sucess">Novo tipo Ramo Atividade <?= $nomeRamoAtividade ?> foi cadastrado.</p>
			<?php }else { ?>
			<p class="alert alert-danger">O tipo de Ramo Atividade <?= $nomeRamoAtividade ?>, não foi cadastrado!</p>
			<?php
		}
		?>
	</div>
</div>
</section>

<?php include 'footer.php'; ?>