<?php include 'cabecalho.php'; ?>
<?php include 'conexao/conecta.php'; ?>

<form name="cadastro-usuario" action="cadastrarUsuario.php" method="post">
  <section>
    <div class="container">
      <div class="row fnd-form">
        <div class="form-group col-md-12">
          <label  for="nomeUsuario">Nome Completo</label>
          <input type="text" class="form-control" name="nomeUsuario" placeholder="inform Seu Nome Completo" data-error="Por favor, Digite seu nome completo." required>
          <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
          <div class="help-block with-errors"></div>        
        </div>
        <div class="form-group col-md-12">
          <label  for="emailUsuario">Email</label>
          <input type="text" class="form-control" name="emailUsuario" placeholder="inform Seu Email" data-error="Por favor, Digite seu Email." required>
          <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
          <div class="help-block with-errors"></div>              
        </div> 
        <div class="form-group col-md-12">
          <label for="tiposUsuario">Indique o tipo de usuario</label>
          <div>


            <label class="checkbox-inline col-md-12">
             <select class="form-control" name="tiposUsuario">
               <?php
          /*
           * Esta linha é responsavel por listar e apresentar informaçoes de usuarios cadastrados;
           */
          $resultado = mysqli_query($con, "select * from TblTipos");
          while($tiposUsuario = mysqli_fetch_assoc($resultado)){ ?>

           <option value="<?=$tiposUsuario['codTipos'];?>">
            <?= $tiposUsuario['nomeTipos']?>
          </option>
          <?php } ?>  
        </select>
      </label>            



    </div>  
  </div>      
  <div class="form-group col-md-12">
    <label for="senhaUsuario">Senha</label>
    <input type="password" class="form-control" name="senhaUsuario" placeholder="Informe Sua Senha, no minimo 8 caracteres" data-error="Por favor, Digite sua Senha." required>
    <div class="help-block with-errors"></div>
  </div>
  <div class="form-group col-md-12">
    <label for="rsenhaUsuario">Confirme sua Senha</label>
    <input type="password" class="form-control" name="rsenhaUsuario" placeholder="Digite novamente sua senha" data-error="Por favor, Digite sua novamente sua senha." required>
    <div class="help-block with-errors"></div>
  </div>
</div>
</div>
</section>
<section>
  <div class="container">
    <div class="row">
      <div class="form-group col-md-12 text-center">
        <button type="submit" class="btn btn-primary">Cadastrar</button>
        <button type="reset" class="btn btn-primary">Limpar</button>
      </div>
    </div>
  </div>
</section>
</form>

<?php include 'footer.php'; ?>



