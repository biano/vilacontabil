<?php
//Recupepera informeçoes do formulario;
$Req = filter_input_array(INPUT_POST, FILTER_DEFAULT);


//funções
function formataString($String){

	return preg_replace("/[^0-9]/", "", $String);
}


function invalidosCPF($Cpf){
	$invalidos = array(
		'00000000000', 
		'11111111111',
		'22222222222',
		'33333333333',
		'44444444444',
		'55555555555',
		'66666666666',
		'77777777777',
		'88888888888',
		'99999999999',
		);
	//verifica e compara se o array CPF fornecido

	if (in_array($Cpf, $invalidos)) {
		return false;
	}else{
		return true;
	}

}


//funcao responsavel por confirmar se o cpf contem 11 digitos
function contaDigitos($Cpf){
	if(strlen($Cpf) != 11){
		return false;
	}else{
		return true;
	}
}

function primeiroDigito($Cpf){
	//variaveis
	$D1 = 0;
	$CPF = $Cpf;
	

	//Multiplica e soma os 10 digitos anteriores
	for ($i = 0; $i < 9; $i++) { 
		$D1 += $CPF[$i] * (10 - $i);
	}

	$Digito = (( $D1 % 11) > 1) ? ( 11 - ($D1 % 11)) : 0; 
	return $Digito;	
}


function segundoDigito($Cpf, $d1){
	//variaveis
	$D1 = $d1;
	$D2 = 0;
	$CPF = $Cpf;
	

	//Multiplica e soma os 10 digitos anteriores
	for ($i = 0; $i < 9; $i++) { 
		$D2 += $CPF[$i] * (11 - $i);
	}

	$Digito = ((($D2+($D1 * 2))% 11) > 1) ? ( 11 - (($D2 + ($D1 * 2)) % 11)) : 0;
	return $Digito;	
}

function validaCPF($Cpf, $d1, $d2){
	if(invalidosCPF($Cpf)){
		$Bool = (substr($Cpf, -2) === $d1.$d2) ? true : false;
		return $Bool;
	}else{
		return false;
	}
}

//variaveis
$CPF = formataString($Req['cpf']);
$primeiroDigito = primeiroDigito($CPF);
$segundoDigito = segundoDigito($CPF, $primeiroDigito);

$validacao = validaCPF($CPF, $primeiroDigito, $segundoDigito);

 
//var_dump($validacao);








