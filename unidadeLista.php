<?php include 'cabecalho.php' ?>
<?php include 'conexao/conecta.php'; ?>
<?php include 'bancoUnidade.php'; ?>

<?php
if (array_key_exists("removido", $_GET) && $_GET["removido"] == "true") {
 ?>
 <section>
  <div class="container">
   <div class="row">
    <p class="alert bg-success">Unidade de Atendimento cancelada com sucesso!</p>	
  </div>
</div>
</section>
<?php
}
?>


<section>
  <div class="container">
   <div class="row">

    <table class="table table-striped table-bordered">

      <thead>
        <tr>
          <th>Unidade de Atendimento</th>
          <th width="10%" style="text-align:center">Novo</th>
          <th width="10%" style="text-align:center">Remover</th>
          <th width="10%" style="text-align:center">Editar</th>
        </tr>
      </thead>
      <tbody>

       <?php
    /*
     * Esta linha é responsavel por listar e apresentar informaçoes de empresas cadastradas;
     */
    
    $unidades = listaUnidades($con);

    foreach ($unidades as $unidade) :
      ?>
    <tr>
      <td><?= $unidade['nomeUnidade']; ?></td>
      <td><a class="btn btn-primary" href="cadUnidade.php">Novo</a></td>
      <td>
        <form action="remove-unidade.php" method="post">
          <input type="hidden" name="codUnidade" value="<?= $unidade['codUnidade']; ?>">
          <button class="btn btn-danger">Remover</button>
        </form>
      </td>
      <td>
        <a class="btn btn-success" href="alterar-unidade.php?id=<?= $unidade['codUnidade'] ?>">Editar</a>     
      </td>
    </tr>
    <?php
    endforeach;
    ?>


  </tbody>
</table>
</div>
</div>
</section>

<?php

include 'footer.php'; 

?>