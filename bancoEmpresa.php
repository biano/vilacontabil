<?php

/*
 * Função responsavel por listar informações armazenadas na tabela: Empresa
 */

function listaEmpresas($con) {
    $empresas = array();

    $result = mysqli_query($con, "select * from empresa");
    while ($empresa = mysqli_fetch_assoc($result)) {
        array_push($empresas, $empresa);
    }
    return $empresas;
}

/*
 * Função responsavel por inserir dados na tabela: Empresa
 */


function inserirEmpresa($con, $NomeEmpresa, $cnpj, $cpf, $NomeResponsavel, $NomeContatoEmpresa, $EmailContatoEmpresa, 
    $TelefoneEmpresa,$CelularEmpresa, $EnderecoEmpresa, $NumeroEmpresa, $BairroEmpresa, $ReferenciaEmpresa) {
    $query = "insert into empresa (nome_empresa,cnpj,cpf, nome_responsavel,nome_contato_empresa,email_contato_empresa, telefone_empresa,celular_empresa,endereco_empresa,numero_empresa,bairro_empresa,referencia_empresa)";
    $query .= "values('$NomeEmpresa','$cnpj', '$cpf', $NomeResponsavel','$NomeContatoEmpresa','$EmailContatoEmpresa', 
    '$TelefoneEmpresa','$CelularEmpresa','$EnderecoEmpresa','$NumeroEmpresa','$BairroEmpresa','$ReferenciaEmpresa')";
   //print $query;
   //die;    
    return mysqli_query($con, $query);
}
/*
* Função responsavel por alterar dados na tabela:
*/
function alterarEmpresa($con, $id, $NomeEmpresa, $cnpj, $cpf, $NomeResponsavel, $NomeContatoEmpresa, $EmailContatoEmpresa,
    $TelefoneEmpresa,$CelularEmpresa, $EnderecoEmpresa, $NumeroEmpresa, $BairroEmpresa, $ReferenciaEmpresa){
    $query = "update empresa set nome_empresa = '{$NomeEmpresa}', cnpj = '{$cnpj}', cpf = '{$cpf}', nome_responsavel ='{$NomeResponsavel}', nome_contato_empresa ='$NomeContatoEmpresa', email_contato_empresa ='{$EmailContatoEmpresa}', telefone_empresa ='{$TelefoneEmpresa}', celular_empresa='{$CelularEmpresa}', endereco_empresa ='{$EnderecoEmpresa}', numero_empresa ='{$NumeroEmpresa}', bairro_empresa='{$BairroEmpresa}', referencia_empresa='{$ReferenciaEmpresa}' where  id_empresa = '{$id}'";
    //print $query;
    //die;
    return mysqli_query($con, $query);
}
/*
* Função responsavel por buscar dados na tabela:
*/
function buscaEmpresa($con, $id){
    $query = "select * from empresa where id_empresa = {$id}";
    $resultado = mysqli_query($con, $query);
    return mysqli_fetch_assoc($resultado);
}
/*
 * Funçao responsavel por remover dados da tabela: agendamento
 */

function removeEmpresas($con, $id){
    $query = "delete from empresa where id_empresa = {$id}";
    return mysqli_query($con, $query);
    
}