<?php include 'cabecalho.php'; ?>
<?php include 'conexao/conecta.php'; ?>

<form name="cadastro-tipo-usuario" action="cadastrarTipoUsuario.php" method="post">
  <section>
    <div class="container">
      <div class="row fnd-form">
        <div class="form-group col-md-12">
          <label  for="nomeTipos">Nome Tipo de Usuario</label>
          <input type="text" class="form-control" name="nomeTipos" placeholder="inform o Tipo de usuario" data-error="Por favor, Digite o Tipo de usuario." required>
          <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
          <div class="help-block with-errors"></div>        
        </div>
      </section>
      <section>
        <div class="container">
          <div class="row">
            <div class="form-group col-md-12 text-center">
              <button type="submit" class="btn btn-primary">Cadastrar</button>
              <button type="reset" class="btn btn-primary">Limpar</button>
            </div>
          </div>
        </div>
      </section>
    </form>

    <?php include 'footer.php'; ?>



