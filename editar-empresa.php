<?php 
	include 'cabecalho.php';
	include 'conexao/conecta.php';
	include 'bancoEmpresa.php'; 


	//variaveis locais
	$id = $_GET['id'];
	$empresa = buscaEmpresa($con, $id);
?>



<section>
	<div id="conteudo" class="container">
		<div class="row">
			<h1>Alterar dados da Empresa</h1>
		</div>
	</section>
	<!-- arquivo que testa conexao com o banco -->

	<form id="formEmpresa" action="alterar-empresa.php" method="POST">
	<input type="hidden" name="id" value="<?=$empresa['id_empresa']?>">
		<section>
			<div class="container">
				<div class="row fnd-form">
					<div class="form-group col-md-4">
						<label for="NomeEmpresa" class="control-label"><h4>Nome Empresa</h4></label>
						<input type="text" class="form-control" id="NomeEmpresa" name="NomeEmpresa" value="<?= $empresa['nome_empresa']?>">
					</div>
					<div class="form-group col-md-4">  
						<label for="cnpj" class="control-label"><h4>CNPJ</h4></label>
						<input type="text" class="form-control" id="cnpj" name="cnpj" value="<?= $empresa['cnpj']?>" >
					</div>
					<div class="form-group col-md-4">  
						<label for="NomeResponsavel"><h4>Nome do Responsàvel</h4></label>
						<input type="text" class="form-control" id="NomeResponsavel" name="NomeResponsavel"
						 value="<?= $empresa['nome_responsavel']?>" >
					</div>
				</div>
			</div> 
		</section>
		<section>
			<div class="container">
				<div class="row fnd-form">
					<div class="form-group col-md-3">
						<label for="EnderecoEmpresa" class="control-label"><h4>Endereço Empresa</h4></label>
						<input type="text" class="form-control" id="EnderecoEmpresa" name="EnderecoEmpresa"
						 value="<?= $empresa['endereco_empresa']?>">
						<div class="help-block with-errors"></div> 
					</div>
					<div class="form-group col-md-1">
						<label><h4>Numero</h4></label>
						<input type="text" class="form-control" id="NumeroEmpresa" name="NumeroEmpresa" value="<?= $empresa['numero_empresa']?>">
					</div>  
					<div class="form-group col-md-4">
						<label><h4>Bairro</h4></label>
						<input type="text" class="form-control" id="BairroEmpresa" name="BairroEmpresa" 
						value="<?= $empresa['bairro_empresa']?>">          
					</div>
					<div class="form-group col-md-4">
						<label><h4>Referencia</h4></label>
						<input type="text" class="form-control" id="ReferenciaEmpresa" name="ReferenciaEmpresa" value="<?= $empresa['referencia_empresa']?>">       
					</div>
				</div>
			</div>
		</section>
		<section>
			<div class="container">
				<div class="row fnd-form"> 
					<div class="form-group col-md-3">
						<label for="NomeContatoEmpresa"><h4>Nome do contato</h4></label>
						<input type="text" class="form-control" id="NomeContatoEmpresa" name="NomeContatoEmpresa" value="<?= $empresa['nome_contato_empresa']?>">
					</div>      
					<div class="form-group col-md-3">
						<label for="EmailContatoEmpresa"><h4>Email do contato</h4></label>
						<input type="text" class="form-control" id="EmailContatoEmpresa" name="EmailContatoEmpresa" value="<?= $empresa['email_contato_empresa']?>">
					</div>             
					<div class="form-group col-md-3">
						<label for="CelularEmpresa"><h4>Celular para contato</h4></label>
						<input type="text" class="form-control" id="CelularEmpresa" name="CelularEmpresa" value="<?= $empresa['celular_empresa']?>">
					</div>
					<div class="form-group col-md-3">
						<label for="TelefoneEmpresa"><h4>Telefone para contato</h4></label>
						<input type="text" class="form-control" id="TelefoneEmpresa" name="TelefoneEmpresa" value="<?= $empresa['telefone_empresa']?>">
					</div>        
				</div>
			</div>
		</section>

		<section>
			<div class="container">
				<div class="row">
					<div class="organiza">
					<button type="submit" class="btn btn-primary">Alterar</button><button type="submit" class="btn btn-primary">Limpar</button>
					</div>
				</div>
			</div>
		</section>
	</form>






	<?php include 'footer.php'; ?>