<?php include 'cabecalho.php'; ?>
<?php include 'conexao/conecta.php'; ?>
<?php include 'bancoEscritorio.php'; ?>

<form name="cadastro-escritorio" action="cadastrarEscritorio.php" method="post">

  <section>

    <div class="container">
      <div class="row fnd-form">
        <h2 class="text-center">Cadastrar Escritorio</h2>
        <div class="form-group col-md-12">
          <label  for="nomeEscritorio">Nome do Escritorio</label>
          <input type="text" class="form-control" name="nomeEscritorio" placeholder="informar o nome do Escritorio" data-error="Por favor, Digite o nome do Escritorio." required>
          <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
          <div class="help-block with-errors"></div>        
        </div>
        <div class="form-group col-md-12">
          <label  for="cnpjEscritorio">CNPJ</label>
          <input type="text" class="form-control" name="cnpjEscritorio" placeholder="inform o CNPJ do Escritorio" data-error="Por favor, Digite o CNPJ do Escritorio." required>
          <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
          <div class="help-block with-errors"></div>
        </div>
        <div class="form-group col-md-12">
          <label  for="enderecoEscritorio">Endereço do Escritorio</label>
          <input type="text" class="form-control" name="enderecoEscritorio" placeholder="inform o Endereço do Escritorio" data-error="Por favor, Digite o Endereço do Escritorio." required>
          <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
          <div class="help-block with-errors"></div>
        </div>
        <div class="form-group col-md-12">
          <label  for="bairroEscritorio">Bairro</label>
          <input type="text" class="form-control" name="bairroEscritorio" placeholder="inform o Bairro" data-error="Por favor, Digite o Bairro." required>
          <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
          <div class="help-block with-errors"></div>
        </div>
        <div class="form-group col-md-12">
          <label  for="referenciaEscritorio">Referencia</label>
          <input type="text" class="form-control" name="referenciaEscritorio" placeholder="inform o referencia do Escritorio" data-error="Por favor, Digite o referencia do Escritorio." required>
          <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
          <div class="help-block with-errors"></div>
        </div>
        <div class="form-group col-md-12">
          <label  for="cepEscritorio">CEP</label>
          <input type="text" class="form-control" name="cepEscritorio" placeholder="inform o CEP do Escritorio" data-error="Por favor, Digite o CEP do Escritorio." required>
          <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
          <div class="help-block with-errors"></div>
        </div>
        <div class="form-group col-md-12">
          <label  for="cidadeEscritorio">Cidade</label>
          <input type="text" class="form-control" name="cidadeEscritorio" placeholder="inform o Cidade do Escritorio" data-error="Por favor, Digite o Cidade do Escritorio." required>
          <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
          <div class="help-block with-errors"></div>
        </div>
        <div class="form-group col-md-12">
          <label  for="estadoEscritorio">Estado</label>
          <input type="text" class="form-control" name="estadoEscritorio" placeholder="inform o estado do Escritorio" data-error="Por favor, Digite o estado do Escritorio." required>
          <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
          <div class="help-block with-errors"></div>
        </div>
        <div class="form-group col-md-12">
          <label  for="ufEscritorio">UF</label>
          <input type="text" class="form-control" name="ufEscritorio" placeholder="inform o UF do Escritorio" data-error="Por favor, Digite o UF do Escritorio." required>
          <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
          <div class="help-block with-errors"></div>
        </div>
      </div>
    </div>
  </section>
  <section>
    <div class="container">
      <div class="row">
        <div class="form-group col-md-12 text-center">
          <button type="submit" class="btn btn-primary">Cadastrar</button>
          <button type="reset" class="btn btn-primary">Limpar</button>
        </div>
      </div>
    </div>
  </section>
</form>

<?php include 'footer.php'; ?>



