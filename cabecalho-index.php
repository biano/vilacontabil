<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title>VILA CONTABIL</title>
	
	<!-- Font Weasome -->
	<link rel="stylesheet" href="assets/css/fontsaw/font-awesome.css">
	<!-- Bootstrap -->
	<link href="assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="assets/css/style.css" rel="stylesheet">

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- <script src="js/jquery-1.2.6.pack.js"></script> -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script> 

 
  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
    </head>
    <body>
    
       <!--Inicio do cabeçalho do sistema -->
     <header>
      <div class="container">
        <!-- linha inicial do cabeçalho sistema  -->
        <div class="row">
          <div id="header-cabeçalho">
           <!-- <div id="icone-header"><i class="fa fa-user fa-3x"></i></div>
           <div id="saudacao-header"> <h3>Olá, <span>Fulano</span></h3></div> -->
           <div id="saudacao-calendar"><?php include 'include/saudacao.php' ?></div>
         </div>
       </div>
       <!-- final linha do cabeçalho -->
     </div>
   </header>
      <nav class="navbar navbar-default">
       <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menuprincipal">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="menuprincipal">
          <ul class="nav navbar-nav">
           <!-- <?php //include "include/menu.php"; ?> -->
         </ul>
         <!-- <ul class="nav navbar-nav navbar-right">
          <li><a href=""><i class="fa fa-unlock-alt" aria-hidden="true"></i></a></li> 
          <?php //include "include/menuUsuario.php" ?>
        </ul-->      
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
  </nav>
  <!-- Cabeçalho do sistema-->