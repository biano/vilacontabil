<?php include 'cabecalho.php' ?>
<?php include 'conexao/conecta.php'; ?>
<?php include 'bancoDepartamento.php'; ?>

<?php
if (array_key_exists("removido", $_GET) && $_GET["removido"] == "true") {
 ?>
 <section>
  <div class="container">
   <div class="row">
    <p class="bg-success">Departamento cancelado com sucesso!</p>	
  </div>
</div>
</section>
<?php
}
?>


<section>
  <div class="container">
   <div class="row">

    <table class="table table-striped table-bordered">

      <thead>
        <tr>
          <th  width="40%">Nome do Departamento</th>
          <th  style="text-align: center">Novo</th>
          <th style="text-align:center">Remover</th>
          <th style="text-align:center">Editar</th>
        </tr>
      </thead>
      <tbody>

       <?php
    /*
     * Esta linha é responsavel por listar e apresentar informaçoes de empresas cadastradas;
     */

    $departamentos = listaDepartamento($con);

    foreach ($departamentos as $departamento) :
      ?>
    <tr>
      <td width="40%"><?= $departamento['nomeDepartamento']; ?></td>
      <td>
        <a type="button" class="btn btn-primary" href="cadDepartamento.php">Novo</a>
      </td>
      <td>
        <form action="remove-departamento.php" method="post">
          <input type="hidden" name="codDepartamento" value="<?= $departamento['codDepartamento']; ?>">
          <button class="btn btn-danger">Remover</button>
        </form>
      </td>
      <td>
        <a class="btn btn-success" href="editar-departamento.php?id=<?= $departamento['codDepartamento'] ?>">Editar</a>     
      </td>
    </tr>
    <?php
    endforeach;
    ?>


  </tbody>
</table>
</div>
</div>
</section>

<?php

include 'footer.php'; 

?>