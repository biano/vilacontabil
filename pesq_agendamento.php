<?php include 'cabecalho.php'; ?>
<?php include 'conexao/conecta.php'; ?>
<?php include 'bancoAgendamento.php'; ?>

<form name="form_pesqAgendamento" action="pesqAgendamento.php" data-toggle="validator" role="form">
	<section>	
		<div class="container">
			<div class="row fnd-form">
				<div class="form-group col-md-4">
					<label for="pesq_agenda_protocolo"><h4>Pesquisar protocolo:</h4></label>
					<input type="text" class="form-control" name="protocolo-txt" placeholder="Informe o protocolo." data-error="Por favor, informe o numero do protocolo!" required>
					<div class="help-block with-errors"></div>
				</div>
				<div class="form-group col-md-4">
					<label for="pesq_agenda_nome"><h4>Pesquisar nome:</h4></label>
					<input type="text" name="nome-txt" class="form-control" name="protocolo-txt" placeholder="Informe o nome." data-error="Por favor, informe o nome do cliente!" required>
					<div class="help-block with-errors"></div>

				</div>
				<div class="form-group col-md-4">
					<label for="pesq_agenda_data"><h4>Pesquisar data:</h4></label>
					<input type="date" name="nome-txt" class="form-control" name="protocolo-txt" placeholder="Informe uma data." data-error="Por favor, informe a data do atendimento!" required>
					<div class="help-block with-errors"></div>					
				</div>
			</div>
		</div>
	</section>
	
	<section>
		<div class="container">
			<!-- botoes -->
			<div class="row">
				<button type="submit" class="btn btn-primary">Pesquisar</button>
				<button type="reset" class="btn btn-primary">Limpar</button>
			</div>
		</div>
	</section>
	
	<section>
		<div class="container">
			<div class="row">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>Protocolo</th>
							<th>Nome Completo</th>
							<th>Data do Pedido</th>
							<th>CEP</th>
							<th>Data Prev. Entrega</th>
							<th>Editar</th>
							<th>Apagar</th>
						</tr>
					</thead>
					<tbody class="striped">
						<?php 

						$agendamento = listaAgendamento($con);
						foreach ($agendamento as $agendados) :				

							?>
						<tr>
							<td><?= $agendados['protocoloPrincipal']; ?></td>
							<td><?= $agendados['nomeSolicitante']; ?></td>
							<td><?= $agendados['dataAtendimento']; ?></td>				
							<td><?= $agendados['cepSolicitante']; ?></td>
							<td><?= $agendados['dataEntrega']; ?></td>
							<td>
								<form action="remove-agendados.php" method="post">
									<input type="hidden" name="id_agendamento" value="<?=$agendados['id_agendamento']?>">
									<button class="btn btn-danger">Remover</button> 
								</form>
							</td>
							<td>
								<form action="editar-agendados.php" method="post">
									<input type="hidden" name="id_agendamento" value="<?=$agendados['id_agendamento']?>">
									<button class="btn btn-success">Editar</button> 
								</form>								
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</section>
</form>

<?php include 'footer.php'; ?>