 
<?php include 'cabecalho.php'; ?>
<?php include 'conexao/conecta.php';?>
<?php include 'bancoEmpresa.php'; ?>

<section>
	<div class="container">
		<div class="row">

				<?php

				function somente_numero( $n ) { return preg_replace("/[^0-9]/", "", $n);}

				$NomeEmpresa = $_POST['NomeEmpresa'];
				$NomeResponsavel = $_POST['NomeResponsavel'];
				$TelefoneEmpresa = somente_numero( $_POST['TelefoneEmpresa'] );
				$CelularEmpresa = somente_numero( $_POST['CelularEmpresa'] );
				$EnderecoEmpresa = $_POST['EnderecoEmpresa'];
				$NumeroEmpresa = somente_numero( $_POST['NumeroEmpresa'] );
				$BairroEmpresa = $_POST['BairroEmpresa'];
				$ReferenciaEmpresa = $_POST['ReferenciaEmpresa'];
				$NomeContatoEmpresa = $_POST['NomeContatoEmpresa'];
				$EmailContatoEmpresa = $_POST['EmailContatoEmpresa'];

				if( strlen($cnpj) > 14 )
					$cnpj = substr( $cnpj, 0, 14 );

				if( strlen($TelefoneEmpresa) > 10 )
					$TelefoneEmpresa = substr( $TelefoneEmpresa, 0, 10 );

				if( strlen($CelularEmpresa) > 11 )
					$CelularEmpresa = substr( $CelularEmpresa, 0, 11 );

				if(inserirEmpresa($con,$NomeEmpresa,$cnpj,$NomeResponsavel,$NomeContatoEmpresa, $EmailContatoEmpresa, $TelefoneEmpresa,$CelularEmpresa,$EnderecoEmpresa,$NumeroEmpresa,$BairroEmpresa,
					$ReferenciaEmpresa)){ ?>
				    <p class="alert-success">Empresa <?= $NomeEmpresa ?>, <?= $NomeResponsavel ?> foi cadastrado.</p>
				<?php }else { ?>
				    <p class="alert-danger">Empresa <?= $NomeEmpresa ?>, não foi cadastrado!</p>
				<?php
				}
				?>

			
		</div>
	</div>
</section>


<?php include 'footer.php'; ?>